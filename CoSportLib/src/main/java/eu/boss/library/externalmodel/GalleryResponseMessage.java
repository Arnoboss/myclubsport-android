package eu.boss.library.externalmodel;

import java.io.Serializable;
import java.util.List;

public class GalleryResponseMessage implements Serializable {

    private static final long serialVersionUID = -2628307407399787992L;

    private TreeNode<String> pathTreeNode;
    private List<String> imagesUrl;

    public TreeNode<String> getPathTreeNode() {
        return pathTreeNode;
    }

    public void setPathTreeNode(TreeNode<String> pathTreeNode) {
        this.pathTreeNode = pathTreeNode;
    }

    public List<String> getImagesUrl() {
        return imagesUrl;
    }

    public void setImagesUrl(List<String> imagesUrl) {
        this.imagesUrl = imagesUrl;
    }

}
