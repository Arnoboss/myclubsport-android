package eu.boss.library.externalmodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentListMessage implements Serializable {

	private static final long serialVersionUID = 1858784687017951043L;
	private final List<CommentMessage> comments = new ArrayList<>();

	public List<CommentMessage> getComments() {
		return comments;
	}

	public void addComment(CommentMessage comment) {
		this.comments.add(comment);
	}

	public void setComment(Collection<CommentMessage> comment) {
		for (CommentMessage n : comment) {
			addComment(n);
		}
	}


}
