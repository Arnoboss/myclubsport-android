package eu.boss.library.externalmodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Generic wrapper class used to respond to smartphones.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GenericResponseMessage<T> implements Serializable {

    private static final long serialVersionUID = 7187581568709145855L;

    private T payLoad;
    private String errorCode;
    private String exceptionMessage;

    public GenericResponseMessage() {
    }

    public GenericResponseMessage(T payLoad) {
        this.payLoad = payLoad;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public T getPayLoad() {
        return payLoad;
    }

    public void setPayLoad(T payLoad) {
        this.payLoad = payLoad;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

}
