package eu.boss.library.externalmodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentMessage implements Serializable {

    private static final long serialVersionUID = -2085223498025429469L;
    // this is the id used in datastore. Migh be helpful
    private Long id;
    private String content;
    private boolean important;
    private boolean notify;
    private Long matchId;
    private int minuteEvent;

    /**
     * Important News which will be displayed in bold in the list
     */
    public CommentMessage() {
    }
    /**
     * Important News which will be displayed in bold in the list
     */
    public CommentMessage(String content, boolean important, Long matchId, int min, Long id) {
        super();
        this.id = id;
        this.content = content;
        this.important = important;
        this.matchId = matchId;
        this.minuteEvent = min;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isImportant() {
        return important;
    }

    public void setImportant(boolean important) {
        this.important = important;
    }

    public boolean isNotify() {
        return notify;
    }

    public void setNotify(boolean notify) {
        this.notify = notify;
    }

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public int getMinuteEvent() {
        return minuteEvent;
    }

    public void setMinuteEvent(int minute) {
        this.minuteEvent = minute;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
