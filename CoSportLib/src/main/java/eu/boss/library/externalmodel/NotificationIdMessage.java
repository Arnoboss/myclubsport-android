package eu.boss.library.externalmodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationIdMessage implements Serializable {

	private static final long serialVersionUID = -5048995456816487842L;

	String notificationId;

	String deviceType;

	String notificationSubscription;

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getNotificationSubscription() {
		return notificationSubscription;
	}

	public void setNotificationSubscription(String notificationSubscription) {
		this.notificationSubscription = notificationSubscription;
	}

}
