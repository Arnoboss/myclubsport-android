package eu.boss.library.externalmodel;

public enum MatchStatus {
	NOT_STARTED, IN_PROGRESS, FIRST_HALF, HALF_TIME, SECOND_HALF, FINISHED, REPORTED;

}
