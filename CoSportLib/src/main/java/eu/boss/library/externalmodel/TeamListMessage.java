package eu.boss.library.externalmodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TeamListMessage implements Serializable {

    private static final long serialVersionUID = -8288584035719301389L;

    private final List<TeamMessage> teams = new ArrayList<>();

    public List<TeamMessage> getTeams() {
        return teams;
    }

    public void setTeams(List<TeamMessage> team) {
        this.teams.clear();
        for (TeamMessage t : team) {
            addTeam(t);
        }
    }

    public void addTeam(TeamMessage team) {
        this.teams.add(team);
    }

}
