package eu.boss.library.externalmodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayerMessage implements Serializable {

	private static final long serialVersionUID = 3043199620578190795L;

	private Long id;
	private String name;
	private int age;
	private PlayerPositionMessage position;
	private List<TeamStatsMessage> teamStats = new ArrayList<TeamStatsMessage>();
	private SeasonMessage season;
	private Category category;

	public PlayerMessage() {
		super();
	}

	public PlayerMessage(String name) {
		super();
		this.name = name;
		this.age = 0;
		position = PlayerPositionMessage.G;
	}

	public PlayerMessage(String name, Category category, int numberOfTeams) {
		super();
		this.name = name;
		this.age = 0;
		this.position = PlayerPositionMessage.G;
		this.category = category;

		for (int i = 0; i < numberOfTeams; i++) {
			teamStats.add(new TeamStatsMessage(TeamNameMessage.values()[i]));
		}

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TeamStatsMessage getTeamStats(int index) {
		return (teamStats != null) ? teamStats.get(index) : null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public PlayerPositionMessage getPosition() {
		return position;
	}

	public void setPosition(PlayerPositionMessage position) {
		this.position = position;
	}

	public List<TeamStatsMessage> getTeamStats() {
		return teamStats;
	}

	public void setTeamStats(List<TeamStatsMessage> teamStats) {
		this.teamStats = teamStats;
		Collections.sort(teamStats);
	}

	public SeasonMessage getSeason() {
		return season;
	}

	public void setSeason(SeasonMessage season) {
		this.season = season;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public String toString() {
		String s = "";
		for (TeamStatsMessage ts : teamStats)
			s += ts.toString();
		return "name: " + getName() + "\n age: " + getAge() + " ans \n" + "position: "
				+ getPosition() + "\n" + s;
	}

	public String toStringHTML() {
		String s = "";
		for (TeamStatsMessage ts : teamStats)
			s += ts.toStringHTML();
		return "name: " + getName() + "</br>" + "age: " + getAge() + " ans </br>" + "position: "
				+ getPosition() + "</br>" + s;
	}

}
