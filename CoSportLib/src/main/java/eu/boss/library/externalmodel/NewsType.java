package eu.boss.library.externalmodel;

public enum NewsType {

    AUTRE("Autre"), CONVOCATION("Convocations"), AGENDA("Agenda"), RESULTAT("Résultats");

    private String label;

    private NewsType(String label) {
        this.label = label;
    }

    public static NewsType getByLabel(String newsTypeStr) {
        for (NewsType n : values()) {
            if (n.label.equals(newsTypeStr)) {
                return n;
            }
        }
        return null;
    }

    public String getLabel() {
        return label;
    }

}
