package eu.boss.library.externalmodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FormationMessage implements Serializable {

    private static final long serialVersionUID = -1099716944992693722L;

    private Long idFormation;
    private String formationName;
    private List<FormationItemMessage> formationItemList;
    private Long matchId;

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public String getFormationName() {
        return formationName;
    }

    public void setFormationName(String formationName) {
        this.formationName = formationName;
    }

    public List<FormationItemMessage> getFormationItemList() {
        return formationItemList;
    }

    public void setFormationItemList(List<FormationItemMessage> formationItemList) {
        this.formationItemList = formationItemList;
    }

    public Long getIdFormation() {
        return idFormation;
    }

    public void setIdFormation(Long idFormation) {
        this.idFormation = idFormation;
    }

    /**
     * @return la liste des remplacants
     */
    public String getRemplacants() {
        String remplacants = "";
        if (formationItemList != null && formationItemList.size() > 11) {
            for (int i = 11; i < formationItemList.size(); i++) {
                if (formationItemList.get(i).getPlayer() != null
                        && formationItemList.get(i).getPlayer().getName() != null) {
                    remplacants += formationItemList.get(i).getPlayer().getName() + " - ";
                }
            }
        }
        return remplacants;
    }

    public void resetFormationIds() {
        this.idFormation = null;
        for (FormationItemMessage item : formationItemList) {
            item.setIdFormationItem(null);
        }

    }
}
