package eu.boss.library.externalmodel;

import android.content.Context;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import eu.boss.library.R;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MatchMessage implements Serializable {

    private static final long serialVersionUID = 4679024802527721145L;
    // this is the id used in datastore. Migh be helpful
    private Long id;
    private Date date;
    private String team1;
    private String team2;
    private int goalsTeam1;
    private int goalsTeam2;
    private String buteursTeam1;
    private String buteursTeam2;
    private String intitule;
    private MatchStatus status;
    private String categoryName;
    private String teamName;
    private Collection<CommentMessage> comments = new ArrayList<CommentMessage>();

    public MatchMessage() {
    }

    public MatchMessage(Date date, String team1, String team2, MatchStatus status, String intitule) {
        super();
        this.date = date;
        this.team1 = team1;
        this.team2 = team2;
        this.status = status;
        this.intitule = intitule;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @JsonIgnore
    public String getFormatedDate() {
        SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);
        return df.format(date);
    }

    @JsonIgnore
    public String getFormatedDateComplete() {

        SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);
        SimpleDateFormat df2 = new SimpleDateFormat("HH:mm", Locale.FRANCE);
        return df.format(date) + " à " + df2.format(date);
    }

    @JsonIgnore
    public String getMatchTime() {
        SimpleDateFormat df2 = new SimpleDateFormat("HH:mm", Locale.FRANCE);
        return df2.format(date);
    }

    @JsonIgnore
    public String getMatchStatusStr(Context context) {
        if (status == MatchStatus.IN_PROGRESS)
            return context.getString(R.string.status_in_progress);
        if (status == MatchStatus.FINISHED)
            return context.getString(R.string.status_finished);
        return "";
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public int getGoalsTeam1() {
        return goalsTeam1;
    }

    public void setGoalsTeam1(int goalsTeam1) {
        this.goalsTeam1 = goalsTeam1;
    }

    public int getGoalsTeam2() {
        return goalsTeam2;
    }

    public void setGoalsTeam2(int goalsTeam2) {
        this.goalsTeam2 = goalsTeam2;
    }

    public String getButeursTeam1() {
        return buteursTeam1;
    }

    public void setButeursTeam1(String buteursTeam1) {
        this.buteursTeam1 = buteursTeam1;
    }

    public String getButeursTeam2() {
        return buteursTeam2;
    }

    public void setButeursTeam2(String buteursTeam2) {
        this.buteursTeam2 = buteursTeam2;
    }

    public MatchStatus getStatus() {
        return status;
    }

    public void setStatus(MatchStatus status) {
        this.status = status;
    }

    public Collection<CommentMessage> getComments() {
        return comments;
    }

    public void setComments(Collection<CommentMessage> comments) {
        this.comments = comments;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    @JsonIgnore
    public String writeScoreSynthese() {
        return team1 + " " + goalsTeam1 + " - " + goalsTeam2 + " " + team2;
    }

}
