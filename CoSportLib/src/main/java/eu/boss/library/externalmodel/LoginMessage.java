package eu.boss.library.externalmodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Message used for https login request and response.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginMessage implements Serializable {

	private static final long serialVersionUID = -498398767055115559L;

	private String login;
	private String password;
	// token utilis� durant X temps en tant qu'identifiant. Celui-ci est stock� sur le t�l�phone et
	// �vite de stocker ID/pwd
	private SecurityTokenMessage token;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public SecurityTokenMessage getToken() {
		return token;
	}

	public void setToken(SecurityTokenMessage token) {
		this.token = token;
	}

}
