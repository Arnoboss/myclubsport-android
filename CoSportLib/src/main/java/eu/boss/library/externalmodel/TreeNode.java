package eu.boss.library.externalmodel;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class TreeNode<T> implements Serializable {
    private static final long serialVersionUID = 268113434749691451L;
    public T data;
    public TreeNode<T> parent;
    public List<TreeNode<T>> children;
    private List<TreeNode<T>> elementsIndex;

    public TreeNode() {
        super();
    }

    @JsonIgnore
    public TreeNode(T data) {
        this.data = data;
        this.children = new LinkedList<TreeNode<T>>();
        this.elementsIndex = new LinkedList<TreeNode<T>>();
        this.elementsIndex.add(this);
    }

    @JsonIgnore
    public boolean isRoot() {
        return parent == null;
    }

    @JsonIgnore
    public boolean isLeaf() {
        return children.size() == 0;
    }

    @JsonIgnore
    public TreeNode<T> addChild(T child) {
        TreeNode<T> childNode = new TreeNode<T>(child);
        childNode.parent = this;
        this.children.add(childNode);
        this.registerChildForSearch(childNode);
        return childNode;
    }

    @JsonIgnore
    public TreeNode<T> addChild(TreeNode<T> childNode) {
        childNode.parent = this;
        this.children.add(childNode);
        this.registerChildForSearch(childNode);
        return childNode;
    }

    @JsonIgnore
    public int getLevel() {
        if (this.isRoot()) return 0;
        else return parent.getLevel() + 1;
    }

    @JsonIgnore
    private void registerChildForSearch(TreeNode<T> node) {
        elementsIndex.add(node);
        if (parent != null) parent.registerChildForSearch(node);
    }

    @JsonIgnore
    public TreeNode<T> findTreeNode(Comparable<T> cmp) {
        for (TreeNode<T> element : this.elementsIndex) {
            T elData = element.data;
            if (cmp.compareTo(elData) == 0) return element;
        }

        return null;
    }

    @Override
    @JsonIgnore
    public String toString() {
        return data != null ? data.toString() : "[data null]";
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public TreeNode<T> getParent() {
        return parent;
    }

    public void setParent(TreeNode<T> parent) {
        this.parent = parent;
    }

    public List<TreeNode<T>> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode<T>> children) {
        this.children = children;
    }

    public List<TreeNode<T>> getElementsIndex() {
        return elementsIndex;
    }

}