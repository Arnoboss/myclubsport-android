package eu.boss.library.externalmodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewsListMessage implements Serializable {

    private static final long serialVersionUID = 8886656922658477137L;
    private final List<NewsMessage> news = new ArrayList<>();

    public List<NewsMessage> getNews() {
        return news;
    }

    public void setNews(Collection<NewsMessage> news) {
        this.news.clear();
        for (NewsMessage n : news) {
            addNews(n);
        }
    }

    public void addNews(NewsMessage news) {
        this.news.add(news);
    }

}
