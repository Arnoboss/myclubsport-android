package eu.boss.library.externalmodel;

public enum Category {
    U7("U7"), U9("U9"), U11("U11"), U13("U13"), U13F("U13 Féminine"), U15("U15"),
    U15F("U15 Féminine"), U17("U17"), U17F("U17 Féminine"), U19("U19"), U19F("U19 Féminine"),
    SENIORS("Séniors"), SENIORSF("Séniors Féminine"), VETERANS("Vétérans");

    private String name = "";

    Category(String name) {
        this.name = name;
    }

    public static Category getByName(String catStr) {
        for (Category c : values()) {
            if (c.name.equals(catStr)) {
                return c;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
