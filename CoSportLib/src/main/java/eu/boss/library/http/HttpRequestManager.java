package eu.boss.library.http;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import eu.boss.library.R;
import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.util.Util;

/**
 * Copyright 2012 Arnaud Bossmann
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Arnaud Bossmann
 */

public class HttpRequestManager {

    private final String RESULT = "result";
    private final String ERROR = "error";
    private HttpURLConnection mConn;
    private ResponseHttpHandler mResponseHandler;
    private ObjectMapper mapper = new ObjectMapper();

    private final Handler handler = new Handler() {

        public void handleMessage(Message msg) {

            String result = msg.getData().getString(RESULT);
            String error = msg.getData().getString(ERROR);
            try {
                GenericResponseMessage<Object> genericMsg = mapper.readValue(result,
                        new TypeReference<GenericResponseMessage<Object>>() {
                        });


                if (mResponseHandler != null) {
                    if ((error == null) && (genericMsg.getErrorCode() == null))
                        mResponseHandler.onSuccess(genericMsg);
                    else {
                        if (error != null)
                            mResponseHandler.onFailure(error, new Exception(error));
                        else
                            mResponseHandler.onFailure(genericMsg.getExceptionMessage(), new Exception(genericMsg.getExceptionMessage()));

                    }
                }
            } catch (Exception e) {
                mResponseHandler.onFailure("Erreur de connexion au serveur. Veuillez réessayer.", e);
            }
        }
    };

    /**
     * init URL connection
     *
     * @param url URL to call
     * @return initialized HttpURLConnection
     * @throws IOException
     */
    private HttpURLConnection initConnection(URL url, boolean securedRequest) throws IOException {
        HttpURLConnection connection;
        if (securedRequest && url.toString().startsWith("https"))
            connection = (HttpsURLConnection) url.openConnection();
        else
            connection = (HttpURLConnection) url.openConnection();

        connection.setDoInput(true);
        connection.setUseCaches(false);
        //connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("Accept-Charset", "utf-8");
        connection.setRequestProperty("User-Agent", Util.USER_AGENT);
        connection.setConnectTimeout(15000); //set timeout to 10 seconds
        connection.setReadTimeout(15000);


        return connection;
    }

    /**
     * Request using client certificate and keystore
     *
     * @param url    URL to call
     * @param toPost message to send
     * @return response as string
     */
    public String httpPost(String url, String toPost, boolean securedRequest) throws Exception {

        URL serverURL = new URL(url);

        mConn = initConnection(serverURL, securedRequest);
        mConn.setDoOutput(true);
        mConn.setRequestMethod("POST");

        Log.d("Post request to url", url);
        Log.d("message to post", toPost);
        // Send the message to post
        OutputStream outputStream = mConn.getOutputStream();
        outputStream.write(toPost.getBytes("UTF-8"));
        outputStream.close();

        // Check for errors
        int responseCode = mConn.getResponseCode();
        InputStream inputStream;
        if (responseCode == HttpURLConnection.HTTP_OK)
            inputStream = mConn.getInputStream();
        else
            inputStream = mConn.getErrorStream();

        // Process the response
        String message = convertStreamToString(inputStream);
        inputStream.close();
        Log.d("Server response", message);
        return message;
    }

    /**
     * Performing get request using client certificate and keystore
     *
     * @param url URL to call
     * @return response as string
     */
    public String httpGet(String url, boolean securedRequest) throws Exception {

        URL serverURL = new URL(url);
        mConn = initConnection(serverURL, securedRequest);
        mConn.setRequestMethod("GET");

        Log.d("Get request to url", url);

        // Check for errors
        int responseCode = mConn.getResponseCode();
        InputStream inputStream;

        if (responseCode == HttpURLConnection.HTTP_OK)
            inputStream = mConn.getInputStream();
        else {
            inputStream = mConn.getErrorStream();
        }

        // Process the response
        String message = convertStreamToString(inputStream);
        Log.d("Server response", message);

        return message;
    }

    /**
     * Perdoming put request using client certificate
     *
     * @param url   URL to call
     * @param toPut message to put
     * @return http response
     */
    public String httpPut(String url, String toPut, boolean securedRequest) throws Exception {
        URL serverURL = new URL(url);
        mConn = initConnection(serverURL, securedRequest);

        mConn.setDoOutput(true);
        mConn.setRequestMethod("PUT");

        Log.d("Put request to url", url);
        Log.d("Put data", toPut);

        // Send the request
        OutputStream outputStream = mConn.getOutputStream();
        outputStream.write(toPut.getBytes("UTF-8"));
        outputStream.close();

        // Check for errors
        int responseCode = mConn.getResponseCode();
        InputStream inputStream;
        if (responseCode == HttpURLConnection.HTTP_OK) {
            inputStream = mConn.getInputStream();
        } else {
            inputStream = mConn.getErrorStream();
        }

        // Process the response
        String message = convertStreamToString(inputStream);
        inputStream.close();
        Log.d("Server response", message);
        return message;
    }

    /**
     * Force http disconection
     */
    public void disconnectHttp() {
        if (mConn != null) {
            mConn.disconnect();
            Log.d("Http request", "disconnected");
        }
    }

    /**
     * Converts InputStream to a String.
     *
     * @param is InputStream to convert
     * @return converted String
     */
    public String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append((line + "\n"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String message = sb.toString();
        if (message.substring(message.length() - 1).compareTo("\n") == 0)
            return message.substring(0, message.length() - 1);
        else
            return message;
    }

    /**
     * Method called for secure requests
     *
     * @param context
     * @param toPost
     * @param url
     * @param method
     * @param securedRequest flag if request is https or not
     * @param rHandler
     */
    public void asyncHttpRequest(final Context context, final String toPost, final String url,
                                 final HttpMethod method, final boolean securedRequest, ResponseHttpHandler rHandler
    ) {
        mResponseHandler = rHandler;
        if (Util.isOnline(context)) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    long requestTime = System.currentTimeMillis();
                    String response = null;
                    String error = null;
                    try {
                        if (method == HttpMethod.GET) {
                            response = httpGet(url, securedRequest);
                        } else if (method == HttpMethod.POST) {
                            response = httpPost(url, toPost, securedRequest);
                        } else {
                            response = httpPut(url, toPost, securedRequest);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("Error HTTP request", e.toString());
                        error = context.getString(R.string.server_error);
                    }
                    requestTime = System.currentTimeMillis() - requestTime;
                    Log.d("Request time", requestTime + " ms");
                    Message msg = Message.obtain();
                    msg.what = 0;

                    // Bundle sent to the handler
                    Bundle b = new Bundle();
                    b.putString(RESULT, response);
                    b.putString(ERROR, error);
                    msg.setData(b);

                    handler.sendMessage(msg);

                }
            }).start();
        } else {
            mResponseHandler.onFailure(context.getString(R.string.no_connection), null);
        }
    }

    /**
     * Method called for unsecure requests
     *
     * @param context
     * @param toPost
     * @param url
     * @param method
     * @param rHandler
     */
    public void asyncHttpRequest(final Context context, final String toPost, final String url,
                                 final HttpMethod method, ResponseHttpHandler rHandler
    ) {
        asyncHttpRequest(context, toPost, url, method, false, rHandler);
    }


}
