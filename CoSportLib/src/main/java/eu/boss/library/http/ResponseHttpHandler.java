package eu.boss.library.http;


import eu.boss.library.externalmodel.GenericResponseMessage;

/**
 * Created by Arnaud on 03/05/2015.
 */
public abstract class ResponseHttpHandler {


    public abstract void onSuccess(GenericResponseMessage<Object> response);

    public abstract void onFailure(String errorResponse, Throwable e);
}
