package eu.boss.library.http;

/**
 * Created by Arnaud on 03/05/2015.
 */
public enum HttpMethod {
    GET, POST, PUT;
}
