package eu.boss.library.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * This class has static methods to save and load data from preference file
 */
public class PrefsManager {

    private final static String NAME = "prefs";

    /**
     * Saves string in prefs
     */
    public static void saveString(Context c, String alias, String toSave) {
        SharedPreferences prefs = c.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(alias, toSave).commit();
        Log.d("String saved in prefs", alias + ": " + toSave);
    }

    /**
     * Saves Int in prefs
     *
     * @param c      Application context
     * @param alias  Alias to save
     * @param toSave value to save
     */

    public static void saveInt(Context c, String alias, int toSave) {
        SharedPreferences prefs = c.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(alias, toSave).commit();
        Log.d("Integer saved in prefs", alias + ": " + toSave);
    }

    /**
     * Loads Int from prefs
     *
     * @param c Application context
     * @return The Integer loaded
     */
    public static int loadInt(Context c, String alias) {
        SharedPreferences prefs = c.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        int toLoad = prefs.getInt(alias, 0);
        Log.d("Integer load from prefs", alias + ": " + toLoad);
        return toLoad;
    }

    /**
     * Loads string from prefs
     *
     * @param c Application context
     * @return The String loaded
     */
    public static String loadString(Context c, String alias) {
        SharedPreferences prefs = c.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        String toLoad = prefs.getString(alias, null);
        Log.d("String load from prefs", alias + ": " + toLoad);
        return toLoad;
    }

    /**
     * Saves Long in prefs
     */
    public static void saveLong(Context c, String alias, long toSave) {
        SharedPreferences prefs = c.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(alias, toSave).commit();
        Log.d("Long saved in prefs", alias + ": " + toSave);
    }

    /**
     * Loads Long from prefs
     *
     * @param c Application context
     * @return The long loaded
     */
    public static long loadLong(Context c, String alias) {
        SharedPreferences prefs = c.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        long toLoad = prefs.getLong(alias, 0);
        Log.d("Long load from prefs", alias + ": " + toLoad);
        return toLoad;
    }

    /**
     * Saves float in prefs
     */
    public static void saveFloat(Context c, String alias, float toSave) {
        SharedPreferences prefs = c.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat(alias, toSave).commit();
        Log.d("Float saved in prefs", alias + ": " + toSave);
    }

    /**
     * Loads float from prefs
     *
     * @param c Application context
     * @return The float loaded
     */
    public static float loadFloat(Context c, String alias) {
        SharedPreferences prefs = c.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        float toLoad = prefs.getFloat(alias, 0);
        Log.d("Float load from prefs", alias + ": " + toLoad);
        return toLoad;
    }

    /**
     * Checks if a boolean value stored in prefs is true or false. False if not present
     *
     * @param c     Application context
     * @param alias Alias to check presence in prefs
     * @return true if the alias is stored otherwise false
     */
    public static boolean isTrue(Context c, String alias) {
        SharedPreferences prefs = c.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        boolean isTrue = prefs.getBoolean(alias, false);
        return isTrue;
    }

    /**
     * Saves a boolean into prefs
     *
     * @param c     Application context
     * @param alias Alias to save as
     * @param value Value to save
     */
    public static void saveBool(Context c, String alias, boolean value) {
        SharedPreferences prefs = c.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(alias, value).commit();
        Log.d("boolean saved in prefs", alias + ": " + value);
    }

    /**
     * Removes a key from the prefs file
     *
     * @param c     Application context
     * @param alias Key of the entry to remove
     */
    public static void removeAlias(Context c, String alias) {
        SharedPreferences prefs = c.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit().remove(alias);
        editor.commit();
        Log.d("Prefs entry removed", alias);
    }

    /**
     * Clears file prefs.xml
     *
     * @param c Application Context
     */
    public static void clearPrefs(Context c) {
        SharedPreferences prefs = c.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear().commit();
        Log.d("Prefs file clear", "Prefs file clear");
    }
}
