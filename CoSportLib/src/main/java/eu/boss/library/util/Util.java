package eu.boss.library.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Arnaud on 29/04/2015. Util methods
 */
public class Util {
    public static final String USER_AGENT = "app=ascoteaux;";

    /**
     * Testing Internet connexion
     *
     * @param c Context where we want to test internet connexion
     * @return true if a connexion is available, else false.
     */
    public static boolean isOnline(Context c) {

        Boolean connected = true;
        ConnectivityManager cm = (ConnectivityManager) c
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        connected = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
        return connected;
    }

    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (Exception e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static String getAppVersionName(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (Exception e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static String currentSeasonDate() {
        Date current = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(current);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        if (month < 7) return year - 1 + "-" + year;

        else return year + "-" + (year + 1);
    }


    /**
     * Resize the bitmap as square.
     *
     * @param bitmap
     * @return
     */
    public static int getSquareCropDimensionForBitmap(Bitmap bitmap) {
        int dimension;
        //If the bitmap is wider than it is tall
        //use the height as the square crop dimension
        if (bitmap.getWidth() >= bitmap.getHeight()) {
            dimension = bitmap.getHeight();
        }
        //If the bitmap is taller than it is wide
        //use the width as the square crop dimension
        else {
            dimension = bitmap.getWidth();
        }
        return dimension;
    }

}
