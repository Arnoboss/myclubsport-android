package eu.boss.mobilesport.util;

/**
 * Created by Arnaud on 03/05/2015.
 */
public class Constant {

    //Numero de projet App engine. utilisé pour GCM
    public static final String SENDER_ID = "385711491694";


    public static final String BASE_URL = "http://miclubdemo.appspot.com";
    public static final String BASE_URL_S = "https://miclubdemo.appspot.com";
    //public static final String BASE_URL = "http://2-dot-ascoteaux.appspot.com";
    //public static final String BASE_URL = "http://ascoteauxdev.appspot.com";
    //public static final String BASE_URL_S = "https://ascoteauxdev.appspot.com";
    //public static final String BASE_URL = "http://192.168.1.24:8888";
    //public static final String BASE_URL_S = "http://192.168.1.24:8888";
    //public static final String BASE_URL = "http://127.0.0.1:8888";
    public static final String NOTIFICATIONS_URL = BASE_URL + "/rest/notifications";
    public static final String PLAYERS_URL = BASE_URL + "/rest/players?category=%s&season=%s";
    public static final String PLAYER_URL = BASE_URL + "/rest/player";
    public static final String RANKING_URL = BASE_URL + "/rest/ranking?category=%s&team=%s";
    public static final String RESULTS_URL = BASE_URL + "/rest/results";
    public static final String CALENDAR_URL = BASE_URL + "/rest/calendar?category=%s&team=%s";
    public static final String AGENDA_URL = BASE_URL + "/rest/agenda";
    public static final String NEWS_URL = BASE_URL + "/rest/news";
    public static final String NEWS_URL_SECURE = BASE_URL_S + "/rest/news";
    public static final String LIVE_MATCH_URL = BASE_URL + "/rest/liveMatch?includeCatAndTeam=true";
    public static final String LIVE_MATCH_COMMENTS_URL = BASE_URL_S + "/securerest/liveMatchComments";
    public static final String PLAYERS_FOR_MATCH_URL = BASE_URL + "/rest/playersForMatch?matchId=%s";
    public static final String LOGIN_URL = BASE_URL_S + "/securerest/login";
    public static final String FORMATION_VISU_URL = BASE_URL + "/faces/formationVisu.xhtml?id=";
    public static final String SINGLE_NEWS_URL = BASE_URL + "/rest/singleNews?id=%s";
    public static final String TEAM_LIST_URL = BASE_URL + "/rest/teams";
    public static final String GALLERY_URL = BASE_URL + "/rest/gallery?folderPath=%s";

    public static final String PARTNERS_URL = BASE_URL + "/partners_content_mobile.html";
    /**
     * PREFS alias
     */
    public static final String LOGIN = "loginId";
    public static final String TOKEN_ID = "tokenId";
    public static final String OLD_TOKEN_ID = "oldTokenId";
    public static final String TOKEN_EXPIRATION_DATE = "tokenExpirationDate";

    /**
     * OTHERS
     */
    public static final String ANDROID = "Android";

    /**
     * PARAMS BETWEEN ACTIVITIES
     */

    public static final String TEAM = "TEAM";
    public static final String URL = "URL";
    public static final String MENUSELECTED = "MENUSELECTED";
    public static final String CATEGORY = "CATEGORY";
    public static final String CALENDAR = "CALENDAR";
    public static final String STATS = "STATS";
    public static final String RANK = "RANK";
    public static final String MATCH = "MATCH";
    public static final String MATCH_ID = "MATCH_ID";
    public static final String SCORER = "SCORER";
    public static final int GOAL_TEAM_1 = 1;
    public static final int GOAL_TEAM_2 = 2;


    /**
     * NOTIFICATIONS
     */
    public static final String MESSAGE = "message";
    public static final String TITLE = "title";
    public static final String URL_IMAGE = "urlImage";
    public static final String TYPE = "type";
    public static final String DETAILS_ID = "detailsId";
    public static final String LIVE = "live";
    public static final String NEWS = "news";
    public static final String URL_NOTIF = "url";

    /**
     * GALERIE PHOTO
     */

    public static final String BUCKET_NAME = "miclubdemo.appspot.com";
    public static final String FOLDER_GALLERY = "gallery/";
    public static final String IMAGE_PUBLIC_PATH;
    public static final String IMAGE_POSITION = "imagePosition";


    static {
        IMAGE_PUBLIC_PATH = "https://storage.googleapis.com/" + BUCKET_NAME + "/";
    }

}
