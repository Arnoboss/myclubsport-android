package eu.boss.mobilesport.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.library.externalmodel.GenericAuthRequestMessage;
import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.externalmodel.LoginMessage;
import eu.boss.library.externalmodel.SecurityTokenMessage;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.library.util.PrefsManager;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.CoteauxApplication;
import eu.boss.mobilesport.news.MainActivity;
import eu.boss.mobilesport.util.Constant;


public class AdminActivity extends AppCompatActivity {

    private EditText etLogin;
    private EditText etPassword;
    private TextView tvError;
    private Button btnConfirm;
    private Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        etLogin = (EditText) findViewById(R.id.etLoginLogin);
        etPassword = (EditText) findViewById(R.id.etPasswordLogin);
        tvError = (TextView) findViewById(R.id.tvLoginError);
        btnConfirm = (Button) findViewById(R.id.btnConfirmLogin);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        View.OnClickListener onClickListener = new View.OnClickListener() {

            public void onClick(View v) {
                if (v.getId() == R.id.btnConfirmLogin) {
                    if ((etLogin.getText().toString().compareTo("") != 0) && (etPassword.getText().toString().compareTo("") != 0)) {
                        performRequest();
                    } else
                        displayErrorMessage(getString(R.string.empty_field));
                } else
                    finish();
            }
        };
        btnConfirm.setOnClickListener(onClickListener);
        btnCancel.setOnClickListener(onClickListener);
    }


    private void performRequest() {
        setEnableViewComponents(false);
        final ObjectMapper mapper = new ObjectMapper();
        HttpRequestManager requestManager = new HttpRequestManager();
        LoginMessage loginMessage = new LoginMessage();
        loginMessage.setLogin(etLogin.getText().toString());
        loginMessage.setPassword(etPassword.getText().toString());
        //On specifie l'ancien token s'il existe
        String oldTokenId = PrefsManager.loadString(AdminActivity.this, Constant.OLD_TOKEN_ID);
        if (oldTokenId != null && !oldTokenId.equals("")) {
            SecurityTokenMessage secToken = new SecurityTokenMessage();
            secToken.setTokenId(oldTokenId);
            loginMessage.setToken(secToken);
        }

        String toPost = "";
        try {
            GenericAuthRequestMessage<LoginMessage> requestMsg = new GenericAuthRequestMessage<LoginMessage>();
            requestMsg.setPayLoad(loginMessage);
            toPost = mapper.writeValueAsString(requestMsg);
        } catch (Exception e) {
            e.printStackTrace();
        }
        requestManager.asyncHttpRequest(AdminActivity.this, toPost, Constant.LOGIN_URL, HttpMethod.POST, true, new ResponseHttpHandler() {
            @Override
            public void onSuccess(GenericResponseMessage<Object> response) {
                setEnableViewComponents(true);
                try {
                    LoginMessage login = mapper.convertValue(response.getPayLoad(), LoginMessage.class);

                    PrefsManager.saveString(AdminActivity.this, Constant.LOGIN, login.getLogin());
                    PrefsManager.saveString(AdminActivity.this, Constant.TOKEN_ID, login.getToken().getTokenId());
                    PrefsManager.saveString(AdminActivity.this, Constant.TOKEN_EXPIRATION_DATE, login.getToken().getExpirationDate());
                    ((CoteauxApplication) getApplication()).getLoginFromPrefs();
                    Intent intent = new Intent(AdminActivity.this, MainActivity.class);
                    if (android.os.Build.VERSION.SDK_INT < 11)
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    else
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                    displayErrorMessage(getString(R.string.error_request));
                }
            }

            @Override
            public void onFailure(String errorResponse, Throwable e) {
                setEnableViewComponents(true);
                displayErrorMessage(errorResponse);
            }
        });
    }

    private void setEnableViewComponents(boolean enable) {
        if (enable)
            findViewById(R.id.flpbAdmin).setVisibility(View.GONE);
        else
            findViewById(R.id.flpbAdmin).setVisibility(View.VISIBLE);
        btnCancel.setEnabled(enable);
        btnConfirm.setEnabled(enable);
        etLogin.setEnabled(enable);
        etPassword.setEnabled(enable);

    }

    private void displayErrorMessage(String errorMsg) {
        tvError.setText(errorMsg);
        findViewById(R.id.llLoginError).setVisibility(View.VISIBLE);
    }

}
