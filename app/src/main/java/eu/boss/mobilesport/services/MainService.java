package eu.boss.mobilesport.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.ArrayList;

import eu.boss.library.externalmodel.Category;
import eu.boss.library.externalmodel.GenericAuthRequestMessage;
import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.externalmodel.NotificationIdMessage;
import eu.boss.library.externalmodel.TeamListMessage;
import eu.boss.library.externalmodel.TeamMessage;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.library.util.Util;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.CoteauxApplication;
import eu.boss.mobilesport.main.SplashScreenActivity;
import eu.boss.mobilesport.util.Constant;

public class MainService extends Service {

    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static String TAG = MainService.class.getName();
    private final IBinder mBinder = new LocalBinder();
    private GoogleCloudMessaging mGcm;
    private String mRegid;

    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }

    /**
     * Register to push notifications
     */
    public void registerGCM() {
        if (Util.isOnline(getApplicationContext())) {
            mGcm = GoogleCloudMessaging.getInstance(this);
            mRegid = getRegistrationId(getApplicationContext());

            if (mRegid.isEmpty()) {
                registerInBackground();
            } else
                // on envoie quand même les données au serveur, au cas où les options d'abonnement aux notifs aient changées
                sendRegistrationIdToBackend();
        } else {
            Log.i(TAG, "No internet connection");
        }

    }

    /**
     * PUSH NOTIFICATIONS_URL PART
     */

    public String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = Util.getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences() {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the registration ID in your app is up to you.
        return getSharedPreferences(SplashScreenActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private void registerInBackground() {

        GCMRegistration gcmRegistration = new GCMRegistration();
        gcmRegistration.execute();
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences();
        int appVersion = Util.getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend() {
        Log.i("splash", "sendRegistrationIdToBackend");
        HttpRequestManager requestManager = new HttpRequestManager();
        final ObjectMapper mapper = new ObjectMapper();
        NotificationIdMessage id = new NotificationIdMessage();
        id.setNotificationId(mRegid);
        id.setDeviceType(Constant.ANDROID);
        //TODO preference de reception des notifications
        //id.setNotificationSubscription("News|A|B|C");
        GenericAuthRequestMessage<NotificationIdMessage> msgToSend = new GenericAuthRequestMessage<>();
        msgToSend.setPayLoad(id);

        ResponseHttpHandler handler = new ResponseHttpHandler() {
            @Override
            public void onSuccess(GenericResponseMessage<Object> response) {
                TeamListMessage teamList = mapper.convertValue(response.getPayLoad(), TeamListMessage.class);
                ArrayList<Category> catList = new ArrayList<>();
                for (TeamMessage t : teamList.getTeams()) {
                    Category c = Category.valueOf(t.getCategory());
                    if (!catList.contains(c)) {
                        catList.add(c);
                    }
                }
                ((CoteauxApplication) getApplication()).setTeamList(teamList);
                ((CoteauxApplication) getApplication()).setCategoryList(catList);
            }

            @Override
            public void onFailure(String errorResponse, Throwable e) {
                Log.e("Notification onFailure", e.toString());
                Toast.makeText(MainService.this, getString(R.string.no_connection) + errorResponse, Toast.LENGTH_SHORT).show();
            }
        };

        try {
            requestManager.asyncHttpRequest(getApplicationContext(), mapper.writeValueAsString(msgToSend), Constant.NOTIFICATIONS_URL,
                    HttpMethod.POST, handler);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    /**
     * Class used for the client Binder. Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public MainService getService() {
            // Return this instance of LocalService so clients can call public
            // methods
            return MainService.this;
        }
    }

    private class GCMRegistration extends AsyncTask<Void, Void, Void> {
        String errorMsg = "";

        @Override
        protected Void doInBackground(Void... params) {

            try {
                if (mGcm == null) {
                    mGcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                }
                mRegid = mGcm.register(Constant.SENDER_ID);
                storeRegistrationId(getApplicationContext(), mRegid);
            } catch (IOException ex) {
                errorMsg = "Error :" + ex.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i(TAG, "mRegid: " + mRegid);
            sendRegistrationIdToBackend();
        }
    }


}
