package eu.boss.mobilesport.partners;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.NavigationDrawerActivity;
import eu.boss.mobilesport.util.Constant;

public class PartnersActivity extends NavigationDrawerActivity {

    private WebView mWebView;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        setTitle(getString(R.string.title_activity_partners));
        displayMenuButton = true;

        mWebView = (WebView) findViewById(R.id.webView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar2);
    }


    @Override
    public void onStart() {
        super.onStart();
        mWebView.setBackgroundColor(Color.WHITE);
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            mWebView.setLayerType(
                    WebView.LAYER_TYPE_SOFTWARE, null);
            mWebView.getSettings().setDisplayZoomControls(false);
        }
        mWebView.getSettings().setAppCacheEnabled(false);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.setScrollbarFadingEnabled(false);
        mWebView.getSettings().setSupportZoom(false);
        mWebView.setBackgroundResource(R.drawable.bg_news_item);
        mWebView.setInitialScale(95);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                mProgressBar.setVisibility(View.GONE);
            }
        });

        mWebView.loadUrl(Constant.PARTNERS_URL);
    }
}
