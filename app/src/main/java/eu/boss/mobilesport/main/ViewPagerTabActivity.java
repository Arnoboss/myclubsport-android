package eu.boss.mobilesport.main;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerTabStrip;

import eu.boss.mobilesport.R;

/**
 * Abstract class to override. Contains data for a ViewPager and PagerTabStrip
 */
public abstract class ViewPagerTabActivity extends NavigationDrawerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webpagertab);

        //Customize the pagerTabStrip colors
        PagerTabStrip pagerTabStrip = (PagerTabStrip) findViewById(R.id.pager_tab_strip);
        pagerTabStrip.setDrawFullUnderline(true);
        pagerTabStrip.setTextColor(Color.WHITE);
        pagerTabStrip.setTabIndicatorColor(Color.RED);
        initViewPager();
    }

    protected abstract void initViewPager();
}
