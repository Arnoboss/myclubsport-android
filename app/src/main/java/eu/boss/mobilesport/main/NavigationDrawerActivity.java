package eu.boss.mobilesport.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import eu.boss.library.externalmodel.Category;
import eu.boss.library.util.PrefsManager;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.admin.AdminActivity;
import eu.boss.mobilesport.fffactivities.AgendaActivity;
import eu.boss.mobilesport.fffactivities.CalendarActivityPager;
import eu.boss.mobilesport.fffactivities.RankingActivityPager;
import eu.boss.mobilesport.fffactivities.ResultsActivity;
import eu.boss.mobilesport.gallery.GalleryTreeViewActivity;
import eu.boss.mobilesport.live.LiveListActivity;
import eu.boss.mobilesport.news.MainActivity;
import eu.boss.mobilesport.partners.PartnersActivity;
import eu.boss.mobilesport.stats.StatsActivity;
import eu.boss.mobilesport.util.Constant;
import eu.boss.mobilesport.util.TeamListActivity;


public abstract class NavigationDrawerActivity extends AppCompatActivity {
    protected boolean displayMenuButton = false;
    protected AlertDialog alertDialog;
    ArrayList<Item> menuContent;
    private String[] drawerItemsList;
    private ListView myDrawer;
    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout drawerLayout;
    private MenuAdapter adapter;

    @Override
    protected void onStart() {
        super.onStart();
        //init views
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (displayMenuButton) {
            mToolbar.setNavigationIcon(R.drawable.ic_menu);
            getSupportActionBar().setHomeButtonEnabled(true);
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerItemsList = getResources().getStringArray(R.array.items);
        myDrawer = (ListView) findViewById(R.id.menu_drawer);

        //fill the adapter
        menuContent = new ArrayList<>();
        menuContent.add(new CustomItem(drawerItemsList[0], R.drawable.news_icon));
        menuContent.add(new CustomItem(drawerItemsList[1], R.drawable.ball_icon));
        menuContent.add(new CustomItem(drawerItemsList[2], R.drawable.results_icon));
        menuContent.add(new CustomItem(drawerItemsList[3], R.drawable.agenda_icon));
        menuContent.add(new CustomItem(drawerItemsList[4], R.drawable.podium_icon));
        menuContent.add(new CustomItem(drawerItemsList[5], R.drawable.calendar_icon));
        menuContent.add(new CustomItem(drawerItemsList[6], R.drawable.stats_icon));
        menuContent.add(new CustomItem(drawerItemsList[7], R.drawable.gallery_icon));
        menuContent.add(new CustomItem(drawerItemsList[8], R.drawable.partners_icon));
        if (((CoteauxApplication) getApplication()).isLogged() &&
                ((CoteauxApplication) getApplication()).getTokenId() != null)
            menuContent.add(new CustomItem(drawerItemsList[10], R.drawable.admin_icon));
        else
            menuContent.add(new CustomItem(drawerItemsList[9], R.drawable.admin_icon));

        adapter = new MenuAdapter(NavigationDrawerActivity.this, menuContent);
        myDrawer.setAdapter(adapter);
        //   myDrawer.setAdapter(new ArrayAdapter<>(this,
        //         R.layout.drawer_item, drawerItemsList));

        myDrawer.setOnItemClickListener(new MyDrawerItemClickListener());

        if (displayMenuButton) {
            mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.app_name, R.string.app_name) {
                public void onDrawerClosed(View view) {
                    invalidateOptionsMenu();
                }

                public void onDrawerOpened(View drawerView) {
                    invalidateOptionsMenu();
                }


            };
            drawerLayout.setDrawerListener(mDrawerToggle);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (displayMenuButton)
            return mDrawerToggle.onOptionsItemSelected(item);
        else {
            finish();
            return true;
        }

    }

    public void displayAlertDialog(String title, String message, String labelBtn1, String labelBtn2) {
        if ((alertDialog == null) || (!alertDialog.isShowing())) {
            try {
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setTitle(title)
                        .setMessage(message);
                if (labelBtn2 == null)
                    builder.setPositiveButton(R.string.ok, null);
                else {

                    builder.setPositiveButton(labelBtn1, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            clickOnPositiveDialog();
                        }
                    })
                            .setNegativeButton(labelBtn2, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    clickOnNegativeDialog();
                                }
                            });

                }
                builder.setIcon(android.R.drawable.ic_dialog_alert);

                alertDialog = builder.show();
            } catch (Exception e) {
                //nothing to do: the activity is not running anymore
            }
        }
    }

    protected void clickOnPositiveDialog() {
    }

    protected void clickOnNegativeDialog() {
    }

    public void updateMenu() {
        adapter.notifyDataSetChanged();
    }

    public void openAlertDisconnect() {
        if ((alertDialog == null) || (!alertDialog.isShowing())) {
            try {
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.disconnect))
                        .setMessage(getString(R.string.confirmDisconnect));

                builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(NavigationDrawerActivity.this, MainActivity.class);
                        if (android.os.Build.VERSION.SDK_INT < 11)
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        else
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        ((CoteauxApplication) getApplication()).setIsLogged(false);
                        ((CoteauxApplication) getApplication()).setTokenId(null);
                        //On sauvegarde l'ancien token pour le désactiver sur le serveur, puis on supprime tout le reste.
                        PrefsManager.saveString(NavigationDrawerActivity.this, Constant.OLD_TOKEN_ID, PrefsManager.loadString(NavigationDrawerActivity.this, Constant.TOKEN_ID));
                        PrefsManager.removeAlias(NavigationDrawerActivity.this, Constant.TOKEN_ID);
                        PrefsManager.removeAlias(NavigationDrawerActivity.this, Constant.TOKEN_EXPIRATION_DATE);
                        startActivity(intent);
                        dialog.cancel();
                    }
                })
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });


                builder.setIcon(android.R.drawable.ic_dialog_alert);

                alertDialog = builder.show();
            } catch (WindowManager.BadTokenException e) {
                //nothing to do: the activity is not running anymore
            }
        }
    }

    /* Base type Item filling the list */
    public interface Item {

        public boolean isSection();
    }

    private class MyDrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapter, View v, int pos, long id) {
            Intent intent = null;
            //Ce boolean s'active lorsqu'on doit afficher la TeamListActivity. SI on n'a qu'une catégory, on affiche directement la page suivante
            boolean displayTeamList = false;
            switch (pos) {
                case 0:
                    intent = new Intent(NavigationDrawerActivity.this, MainActivity.class);
                    break;
                case 1:
                    intent = new Intent(NavigationDrawerActivity.this, LiveListActivity.class);
                    break;
                case 2:
                    intent = new Intent(NavigationDrawerActivity.this, ResultsActivity.class);
                    break;
                case 3:
                    intent = new Intent(NavigationDrawerActivity.this, AgendaActivity.class);
                    break;
                case 4:
                    if ((((CoteauxApplication) getApplication()).getCategoryList().size() == 0) || (((CoteauxApplication) getApplication()).getCategoryList().size() > 1)) {
                        displayTeamList = true;
                        intent = new Intent(NavigationDrawerActivity.this, TeamListActivity.class);
                        intent.putExtra(Constant.MENUSELECTED, Constant.RANK);
                    } else {
                        intent = new Intent(NavigationDrawerActivity.this, RankingActivityPager.class);
                        Category c = Category.valueOf(((CoteauxApplication) getApplication()).getTeamList().getTeams().get(0).getCategory());
                        intent.putExtra(Constant.CATEGORY, c);
                    }
                    break;
                case 5:
                    if ((((CoteauxApplication) getApplication()).getCategoryList().size() == 0) || (((CoteauxApplication) getApplication()).getCategoryList().size() > 1)) {
                        displayTeamList = true;
                        intent = new Intent(NavigationDrawerActivity.this, TeamListActivity.class);
                        intent.putExtra(Constant.MENUSELECTED, Constant.CALENDAR);
                        break;
                    } else {
                        intent = new Intent(NavigationDrawerActivity.this, CalendarActivityPager.class);
                        Category c = Category.valueOf(((CoteauxApplication) getApplication()).getTeamList().getTeams().get(0).getCategory());
                        intent.putExtra(Constant.CATEGORY, c);
                    }
                case 6:
                    if ((((CoteauxApplication) getApplication()).getCategoryList().size() == 0) || (((CoteauxApplication) getApplication()).getCategoryList().size() > 1)) {
                        displayTeamList = true;
                        intent = new Intent(NavigationDrawerActivity.this, TeamListActivity.class);
                        intent.putExtra(Constant.MENUSELECTED, Constant.STATS);
                    } else {
                        intent = new Intent(NavigationDrawerActivity.this, CalendarActivityPager.class);
                        Category c = Category.valueOf(((CoteauxApplication) getApplication()).getTeamList().getTeams().get(0).getCategory());
                        intent.putExtra(Constant.CATEGORY, c);
                    }
                    break;
                case 7:
                    intent = new Intent(NavigationDrawerActivity.this, GalleryTreeViewActivity.class);
                    break;
                case 8:
                    intent = new Intent(NavigationDrawerActivity.this, PartnersActivity.class);
                    break;
                case 9:
                    if (((CoteauxApplication) getApplication()).isLogged() &&
                            ((CoteauxApplication) getApplication()).getTokenId() != null)
                        openAlertDisconnect();
                    else
                        intent = new Intent(NavigationDrawerActivity.this, AdminActivity.class);
                    break;
                default:
                    break;
            }
            if (intent != null) {
                if (displayTeamList) {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                } else
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

                startActivity(intent);
            }
            drawerLayout.closeDrawer(myDrawer);
        }
    }

    /* Menu item */
    private class CustomItem implements Item {

        public String tag;
        public int iconRes;

        public CustomItem(String tag, int iconRes) {
            this.tag = tag;
            this.iconRes = iconRes;
        }

        @Override
        public boolean isSection() {
            return false;
        }

    }

    /**
     * Adapter filling the MenuList
     */
    public class MenuAdapter extends ArrayAdapter<Item> {

        private ArrayList<Item> items;
        private LayoutInflater vi;

        public MenuAdapter(Context context, ArrayList<Item> items) {
            super(context, 0, items);
            this.items = items;
            vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            final Item i = items.get(position);
            if (i != null) {
                // Different behavior for section or CustomItems
                CustomItem ei = (CustomItem) i;
                v = vi.inflate(R.layout.drawer_item, null);
                ImageView icon = (ImageView) v.findViewById(R.id.iv_menu);
                icon.setImageResource(ei.iconRes);
                TextView title = (TextView) v.findViewById(R.id.tv_menu);
                title.setText(ei.tag);
            }

            return v;
        }

        @Override
        public boolean isEnabled(int position) {
            final Item i = items.get(position);
            if (i.isSection()) return false;
            else return true;
        }
    }

}
