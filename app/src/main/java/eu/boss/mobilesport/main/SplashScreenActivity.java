package eu.boss.mobilesport.main;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import eu.boss.library.util.Util;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.news.MainActivity;
import eu.boss.mobilesport.services.MainService;


public class SplashScreenActivity extends Activity {


    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private final static String TAG = SplashScreenActivity.class.getName();

    private boolean isBound = false;
    private MainService mService;

    protected ServiceConnection mControllerConnection = new ServiceConnection() {

        // Called when the connection with the service is established
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = ((MainService.LocalBinder) service)
                    .getService();
            isBound = true;
            // Check device for Play Services APK.
            if (checkPlayServices()) {
                mService.registerGCM();
            }
            startWaitCount();
        }

        // Called when the connection with the service disconnects unexpectedly
        public void onServiceDisconnected(ComponentName className) {
            mService = null;
            isBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        TextView tvVersion = (TextView) findViewById(R.id.tvAppVersion);
        tvVersion.setText("Version " + Util.getAppVersionName(this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, MainService.class);
        isBound = bindService(intent, mControllerConnection, Context.BIND_AUTO_CREATE);
        Log.i(TAG, "" + isBound);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Check device for Play Services APK.
        checkPlayServices();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isBound == true) unbindService(mControllerConnection);
    }


    /**
     * Start counting before going on the next screen
     * init the application. Waiting 2.5 seconds on the splash screen
     */
    private void startWaitCount() {
        Handler h = new Handler();
        h.postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, 1200);
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }


}
