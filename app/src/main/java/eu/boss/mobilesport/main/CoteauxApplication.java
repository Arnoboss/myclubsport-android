package eu.boss.mobilesport.main;

import android.app.Application;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import eu.boss.library.externalmodel.Category;
import eu.boss.library.externalmodel.TeamListMessage;
import eu.boss.library.externalmodel.TeamMessage;
import eu.boss.library.util.PrefsManager;
import eu.boss.mobilesport.util.Constant;

/**
 * La partie "Login" est relativement simple et peu s�curis�e. Vu la population vis�e (20-30 personnes) ce n'est pas grave
 * Created by Arnaud on 15/05/2015.
 */
public class CoteauxApplication extends Application {

    private boolean isLogged = false;
    private String tokenId;
    private TeamListMessage teamList;
    private ArrayList<Category> categoryList = new ArrayList<>();
    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    //Pseudo cache for current team compo. Key: matchId; value: playersList;
    private HashMap<Long, List<String>> playersMap = new HashMap<>();

    @Override
    public void onCreate() {
        super.onCreate();
        //Image loader initialization: enable image cache to be faster
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);

        getLoginFromPrefs();

    }

    /**
     * Load tokenId. Token expiration has been removed in version 2.0
     */
    public void getLoginFromPrefs() {

        tokenId = PrefsManager.loadString(this, Constant.TOKEN_ID);
        /*String dateString = PrefsManager.loadString(this, Constant.TOKEN_EXPIRATION_DATE);
        try {
            Date date = formatter.parse(dateString);
            if (date.compareTo(new Date()) > 0) {*/
                if (tokenId != null && !tokenId.equals(""))
                    isLogged = true;
            /*} else {
                //si le token est expiré, on logout automatiquement l'appli. On stocke l'ancien token dans OLD_TOKEN_ID pour pouvoir le spécifier lors du prochain login
                PrefsManager.saveString(this, Constant.OLD_TOKEN_ID, tokenId);
                PrefsManager.removeAlias(this, Constant.TOKEN_ID);
                PrefsManager.removeAlias(this, Constant.TOKEN_EXPIRATION_DATE);
            }
        } catch (Exception e) {
            //nothing to do, login will be false
        }*/
    }

    public int getNumberOfTeamInCategory(Category c) {
        int count = 0;
        Iterator<TeamMessage> it = teamList.getTeams().iterator();
        while (it.hasNext()) {
            TeamMessage team = it.next();
            if (team.getCategory().equals(c.name()))
                count++;
        }
        return count;
    }

    public boolean isLogged() {
        return isLogged;
    }

    public void setIsLogged(boolean isLogged) {
        this.isLogged = isLogged;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public TeamListMessage getTeamList() {
        return teamList;
    }

    public void setTeamList(TeamListMessage teamList) {
        this.teamList = teamList;
    }

    public ArrayList<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(ArrayList<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public List<String> getPlayersForMatch(Long matchId) {
        return playersMap.get(matchId);
    }

    public void setPlayersForMatch(List<String> players, Long matchId) {
        playersMap.put(matchId, players);
    }
}
