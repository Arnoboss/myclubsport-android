package eu.boss.mobilesport.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;

import com.google.android.gms.gcm.GcmListenerService;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import eu.boss.mobilesport.R;
import eu.boss.mobilesport.live.LiveDetailsActivity;
import eu.boss.mobilesport.live.LiveListActivity;
import eu.boss.mobilesport.main.SplashScreenActivity;
import eu.boss.mobilesport.news.MainActivity;
import eu.boss.mobilesport.news.NewsDetailsActivity;
import eu.boss.mobilesport.util.Constant;

/**
 * Created by Arnaud on 30/04/2015.
 */
public class GcmIntentService extends GcmListenerService {

    public static final int NOTIFICATION_ID = 1;
    private final static String TAG = GcmIntentService.class.getName();
    private NotificationManager mNotificationManager;

    public GcmIntentService() {
    }

    @Override
    public void onMessageReceived(String from, Bundle extras) {
        Log.i(TAG, "Push received");
        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            // Post notification of received message.
            String title = extras.getString(Constant.TITLE);
            //On remplace les caracteres html par du texte normal
            String msg = extras.getString(Constant.MESSAGE);
            if (msg != null)
                msg = msg.replaceAll("\\<[^>]*>", "");
            String type = extras.getString(Constant.TYPE);
            String urlImage = extras.getString(Constant.URL_IMAGE);
            String detailsId = extras.getString(Constant.DETAILS_ID);
            sendNotification(title, msg, type, urlImage, detailsId);
            Log.i(TAG, "Received: " + extras.toString());

        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        //GcmBroadcastReceiver.completeWakefulIntent(intent);
    }


    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String title, String msg, String type, String urlImage, String detailsId) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent backIntent;
        final PendingIntent contentIntent;

        if (type != null) {
            Intent i;
            if (type.equals(Constant.LIVE)) {
                if (detailsId != null) {
                    backIntent = new Intent(getApplicationContext(), LiveListActivity.class);
                    backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    backIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i = new Intent(this, LiveDetailsActivity.class);
                    i.putExtra(Constant.DETAILS_ID, detailsId);
                    contentIntent = PendingIntent.getActivities(this, 0,
                            new Intent[]{backIntent, i}, PendingIntent.FLAG_ONE_SHOT);
                } else {
                    i = new Intent(this, LiveListActivity.class);
                    contentIntent = PendingIntent.getActivity(this, 0,
                            i, 0);
                }


            } else if (type.equals(Constant.NEWS)) {
                if (detailsId != null) {
                    backIntent = new Intent(getApplicationContext(), MainActivity.class);
                    backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    backIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i = new Intent(this, NewsDetailsActivity.class);
                    i.putExtra(Constant.DETAILS_ID, detailsId);
                    contentIntent = PendingIntent.getActivities(this, 0,
                            new Intent[]{backIntent, i}, PendingIntent.FLAG_ONE_SHOT);
                } else {
                    i = new Intent(this, SplashScreenActivity.class);
                    contentIntent = PendingIntent.getActivity(this, 0,
                            i, 0);
                }

                //} else if (type.equals(Constant.URL_NOTIF)) {
                //TODO: ouvrir le navigateur
            } else {
                contentIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, SplashScreenActivity.class), 0);
            }
        } else
            contentIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, SplashScreenActivity.class), 0);

        final NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.status_icon)
                        .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher))
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setVibrate(new long[]{250, 250, 250})
                        .setLights(Color.BLUE, 1000, 1000)
                        .setContentText(msg);

        if (urlImage == null || urlImage.equals("")) {
            mBuilder.setContentIntent(contentIntent);
            mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                    .bigText(msg));
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        } else {
            ImageLoader.getInstance().loadImage(urlImage, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    mBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                            .bigPicture(loadedImage));
                    mBuilder.setContentIntent(contentIntent);
                    mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
                }
            });
        }


    }
}
