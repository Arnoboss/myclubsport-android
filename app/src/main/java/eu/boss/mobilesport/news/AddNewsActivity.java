package eu.boss.mobilesport.news;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import eu.boss.library.externalmodel.GenericAuthRequestMessage;
import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.externalmodel.ImageDetails;
import eu.boss.library.externalmodel.NewsRequestMessage;
import eu.boss.library.externalmodel.NewsType;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.library.util.PrefsManager;
import eu.boss.library.util.ScalingUtilities;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.NavigationDrawerActivity;
import eu.boss.mobilesport.util.Constant;

public class AddNewsActivity extends NavigationDrawerActivity implements View.OnClickListener {

    private static final String JPG = "jpg";
    private static final String JPEG = "jpeg";
    private static final String PNG = "png";

    private static final int SELECT_PICTURE = 1;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 2;
    private EditText etTitle;
    private EditText etContent;
    private NewsRequestMessage newsMessage;
    private View progressView;
    private Spinner spinerNewsType;
    private ObjectMapper mapper = new ObjectMapper();
    private Uri imageUri;

    public static byte[] readFully(InputStream input) throws IOException {
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
        return output.toByteArray();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_news);
        this.etTitle = (EditText) findViewById(R.id.etAddNewsTitle);
        this.etContent = (EditText) findViewById(R.id.etAddNewsContent);
        etContent.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.etAddNewsContent) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        this.progressView = findViewById(R.id.flpbAddNews);
        findViewById(R.id.btnAddImage).setOnClickListener(this);
        findViewById(R.id.btnSendNews).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayAlertDialog(getString(R.string.comment_confirm_title_news), getString(R.string.comment_confirm_news), getString(R.string.confirm), getString(R.string.cancel));
            }
        });
        spinerNewsType = (Spinner) findViewById(R.id.spinnerNewsType);

        //On masque l'ajout d'image si le type est different de 'autre'
        spinerNewsType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).toString().equals(NewsType.AUTRE.getLabel()))
                    findViewById(R.id.llAddImage).setVisibility(View.VISIBLE);
                else

                    findViewById(R.id.llAddImage).setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void onClick(View v) {
        if (v.getId() == R.id.btnAddImage) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Cela signifie que la permission à déjà était
                    //demandé et l'utilisateur l'a refusé
                    //Vous pouvez aussi expliquer à l'utilisateur pourquoi
                    //cette permission est nécessaire et la redemander
                    displayAlertDialog(getString(R.string.permission_denied_title), getString(R.string.permission_denied_storage), getString(R.string.ok), null);
                } else {
                    //Sinon demander la permission
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
            } else
                chooseImage();

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // La permission est garantie
                    chooseImage();
                } else {
                    // La permission est refusée
                    displayAlertDialog(getString(R.string.permission_denied_title), getString(R.string.permission_denied_storage), getString(R.string.ok), null);
                }
                return;
            }
        }
    }

    private void chooseImage() {
        Intent pickIntent = new Intent();
        pickIntent.setType("image/*");
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);

        ContentValues values = new ContentValues();
        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

        String pickTitle = getString(R.string.select_picture); //
        Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);
        chooserIntent.putExtra
                (
                        Intent.EXTRA_INITIAL_INTENTS,
                        new Intent[]{takePhotoIntent}
                );

        startActivityForResult(chooserIntent, SELECT_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PICTURE && resultCode == Activity.RESULT_OK) {
            String imageName, mimeType, fileType;
            Bitmap originalBmp;
            try {
                if (data == null) {
                    //l'image vient de la camera
                    originalBmp = MediaStore.Images.Media.getBitmap(
                            getContentResolver(), imageUri);
                    String[] splittedPath = imageUri.getPath().split("/");
                    imageName = splittedPath[splittedPath.length - 1];
                    mimeType = "image/jpeg";
                    fileType = "jpg";

                } else {
                    //l'image vient de la galerie
                    fileType = getType(data.getData());
                    if (fileType != null && (fileType.toLowerCase().equals(JPEG) || fileType.toLowerCase().equals(JPG) || fileType.toLowerCase().equals(PNG))) {

                        InputStream inputStream = getContentResolver().openInputStream(data.getData());
                        Log.d("get image ", data.getData().getPath());
                        Log.d("get mime type ", getMimeType(data.getData()));
                        originalBmp = BitmapFactory.decodeStream(inputStream);
                        String[] splittedPath = data.getData().getPath().split("/");
                        imageName = splittedPath[splittedPath.length - 1];
                        mimeType = getMimeType(data.getData());
                    } else {
                        displayAlertDialog(getString(R.string.error_title), getString(R.string.error_format_picture), null, null);
                        return;
                    }
                }
                Bitmap scalledBmp = ScalingUtilities.createScaledBitmap(originalBmp, 1500, 1500, ScalingUtilities.ScalingLogic.FIT);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                scalledBmp.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                byte[] byteArray = stream.toByteArray();

                ImageDetails img = new ImageDetails();
                img.setImageName(imageName + "." + fileType);
                img.setImageMimeType(mimeType);
                img.setImageAsBytes(Base64.encode(byteArray, Base64.DEFAULT));
                ((TextView) findViewById(R.id.tvImageName)).setText(img.getImageName());
                if (newsMessage == null) {
                    newsMessage = new NewsRequestMessage();
                }
                newsMessage.setImage(img);

            } catch (Exception e) {
                Log.e("Error getting image: ", e.toString());
            }
        } //else
        //displayAlertDialog(getString(R.string.error_title), getString(R.string.error_picture), null, null);
    }

    public String getType(Uri uriImage) {
        ContentResolver cR = AddNewsActivity.this.getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uriImage));
    }


    public String getMimeType(Uri uriImage) {
        ContentResolver cR = AddNewsActivity.this.getContentResolver();
        return cR.getType(uriImage);
    }

    private void postNewComment() {

        try {
            progressView.setVisibility(View.VISIBLE);
            findViewById(R.id.btnSendNews).setEnabled(false);
            GenericAuthRequestMessage<NewsRequestMessage> requestMsg = new GenericAuthRequestMessage<>();
            requestMsg.setPayLoad(newsMessage);
            requestMsg.setTokenId(PrefsManager.loadString(AddNewsActivity.this, Constant.TOKEN_ID));
            requestMsg.setUserId(PrefsManager.loadString(AddNewsActivity.this, Constant.LOGIN));

            HttpRequestManager requestManager = new HttpRequestManager();

            ResponseHttpHandler handler = new ResponseHttpHandler() {
                @Override
                public void onSuccess(GenericResponseMessage<Object> response) {

                    try {
                        findViewById(R.id.btnSendNews).setEnabled(true);
                        progressView.setVisibility(View.GONE);
                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                        displayAlertDialog(getString(R.string.error_title), getString(R.string.error_request), null, null);
                    }
                }

                @Override
                public void onFailure(String errorResponse, Throwable e) {
                    findViewById(R.id.btnSendNews).setEnabled(true);
                    progressView.setVisibility(View.GONE);
                    displayAlertDialog(getString(R.string.error_title), errorResponse, null, null);
                }
            };

            requestManager.asyncHttpRequest(AddNewsActivity.this, mapper.writeValueAsString(requestMsg), Constant.NEWS_URL_SECURE, HttpMethod.POST, true, handler);
        } catch (Exception e) {
            findViewById(R.id.btnSendNews).setEnabled(true);
            Log.e("Error sending news", e.toString());
        }
    }

    @Override
    public void displayAlertDialog(String title, String message, String labelBtn1, String labelBtn2) {

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle(title)
                    .setMessage(message);
            if (labelBtn2 == null)
                builder.setPositiveButton(R.string.ok, null);
            else {

                builder.setPositiveButton(labelBtn1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        clickOnPositiveDialog();
                    }
                })
                        .setNegativeButton(labelBtn2, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                clickOnNegativeDialog();
                            }
                        });

            }
            builder.setIcon(android.R.drawable.ic_dialog_alert);

            alertDialog = builder.show();
        } catch (WindowManager.BadTokenException e) {
            //nothing to do: the activity is not running anymore
        }
    }

    @Override
    public void clickOnPositiveDialog() {
        if (etTitle.getText().toString() == null || etTitle.getText().toString().equals("") || etContent.getText().toString() == null || etContent.getText().toString().equals("")) {
            displayAlertDialog(getString(R.string.error_title), getString(R.string.empty_field), null, null);
        } else if (spinerNewsType.getSelectedItem().toString().equals(NewsType.AUTRE.getLabel()) && (newsMessage == null || newsMessage.getImage().getImageAsBytes() == null)) {
            displayAlertDialog(getString(R.string.error_title), getString(R.string.empty_image), null, null);
        } else {
            if (newsMessage == null)
                newsMessage = new NewsRequestMessage();

            newsMessage.setTitle(etTitle.getText().toString());
            newsMessage.setContent(etContent.getText().toString());
            newsMessage.setNewsType(NewsType.getByLabel(spinerNewsType.getSelectedItem().toString()));
            newsMessage.setNotify(((CheckBox) findViewById(R.id.checkBoxAddNewsNotify)).isChecked());
            postNewComment();
        }
    }
}
