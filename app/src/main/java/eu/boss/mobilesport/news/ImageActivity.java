package eu.boss.mobilesport.news;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import eu.boss.mobilesport.R;

public class ImageActivity extends AppCompatActivity {

    public static final String IMAGE_URL = "imageUrl";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        final SubsamplingScaleImageView imageView = (SubsamplingScaleImageView) findViewById(R.id.imageView);
        String urlImage = getIntent().getExtras().getString(IMAGE_URL);
        ImageLoader.getInstance().loadImage(urlImage, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                findViewById(R.id.flpbImage).setVisibility(View.GONE);
                imageView.setImage(ImageSource.bitmap(loadedImage));

            }
        });


    }

}
