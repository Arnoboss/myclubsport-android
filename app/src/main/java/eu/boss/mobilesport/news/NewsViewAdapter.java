package eu.boss.mobilesport.news;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import eu.boss.library.externalmodel.NewsListMessage;
import eu.boss.library.externalmodel.NewsMessage;
import eu.boss.mobilesport.R;

/**
 * Created by Arnaud on 26/05/2015.
 */
public class NewsViewAdapter extends BaseAdapter {


    protected ImageLoader imageLoader = ImageLoader.getInstance();
    private NewsListMessage mNewsList;
    private LayoutInflater inflater;


    public NewsViewAdapter(Context context, NewsListMessage newsList
    ) {
        inflater = LayoutInflater.from(context);
        mNewsList = newsList;
    }

    @Override
    public int getCount() {
        return mNewsList.getNews().size();
    }

    @Override
    public Object getItem(int position) {
        return mNewsList.getNews().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        NewsMessage news = mNewsList.getNews().get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.news_item, null);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_news_title);
            //holder.tvContent = (TextView) convertView.findViewById(tvMerchantItemNameId);
            holder.tvDate = (TextView) convertView.findViewById(R.id.tv_news_date);
            holder.ivImage = (ImageView) convertView.findViewById(R.id.iv_news_image);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        if (news.getImageUrl() != null) {
            imageLoader.displayImage(news.getImageUrl(), holder.ivImage);
        } else holder.ivImage.setImageBitmap(null);

        /**if (news.getImageUrl() != null) {
            imageLoader.loadImage(news.getImageUrl(), new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    int dimension = Util.getSquareCropDimensionForBitmap(loadedImage);
                    holder.ivImage.setImageBitmap(ThumbnailUtils.extractThumbnail(loadedImage, dimension, dimension));

                }
            });

            //imageLoader.displayImage(news.getImageUrl(), holder.ivImage);
         } else holder.ivImage.setImageBitmap(null);*/

        // holder.tvContent.setText(news.getContent());
        holder.tvTitle.setText(news.getTitle());
        holder.tvDate.setText(news.getFormatedDate());

        return convertView;
    }

    public void addAll(NewsListMessage list) {
        this.mNewsList.getNews().addAll(list.getNews());
        notifyDataSetChanged();
    }

    public void clear() {
        mNewsList.getNews().clear();
    }

    public void replace(NewsListMessage list) {
        clear();
        addAll(list);
    }

    public NewsMessage getNews(int position) {
        return mNewsList.getNews().get(position);
    }


    private class ViewHolder {

        TextView tvTitle;
        TextView tvContent;
        TextView tvDate;
        ImageView ivImage;
    }
}
