package eu.boss.mobilesport.news;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nostra13.universalimageloader.core.ImageLoader;

import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.externalmodel.NewsMessage;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.NavigationDrawerActivity;
import eu.boss.mobilesport.util.Constant;

public class NewsDetailsActivity extends NavigationDrawerActivity {
    public static final String NEWS = "NEWS";
    public static final String NEWS_ID = "newsId";

    private NewsMessage news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        if (getIntent().getSerializableExtra(NEWS) != null)
            news = (NewsMessage) getIntent().getSerializableExtra(NEWS);
        if (getIntent().getStringExtra(Constant.DETAILS_ID) != null) {
            //FROM PUSH NOTIFICATION
            getSingleNews(getIntent().getStringExtra(Constant.DETAILS_ID));
            findViewById(R.id.flpbNewsDetails).setVisibility(View.VISIBLE);
        } else if ((getIntent().getData() != null) && getIntent().getData().getQueryParameter(NEWS_ID) != null) {
            //FROM URI
            getSingleNews(getIntent().getData().getQueryParameter(NEWS_ID));
            findViewById(R.id.flpbNewsDetails).setVisibility(View.VISIBLE);

        } else {
            fillNews();
        }

    }

    //Fill webview with news infos
    private void fillNews() {
        final ImageView ivNews = (ImageView) findViewById(R.id.ivNewsDetails);

        if (news.getImageUrl() != null) {
            ImageLoader.getInstance().displayImage(news.getImageUrl(), ivNews);
        }

        WebView webView = (WebView) findViewById(R.id.wvNewsDetailsContent);
        ((TextView) findViewById(R.id.tvNewsDetailsDate)).setText(news.getFormatedDate());
        ((TextView) findViewById(R.id.tvNewsDetailsTitle)).setText(news.getTitle());


        webView.setBackgroundColor(getResources().getColor(R.color.bg_light_gray));
        if (android.os.Build.VERSION.SDK_INT >= 11) webView.setLayerType(
                WebView.LAYER_TYPE_SOFTWARE, null);

        webView.clearCache(true);
        webView.getSettings().setJavaScriptEnabled(true);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            webView.setLayerType(WebView.LAYER_TYPE_NONE, null);
        }
        //utilise pour ouvrir les liens dans le browser
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null && (url.startsWith("http://") || url.startsWith("https://"))) {
                    view.getContext().startActivity(
                            new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                } else {
                    return false;
                }
            }
        });

        webView.loadData(news.getContent(), "text/html; charset=UTF-8", null);
        webView.getSettings().setDefaultFontSize(15);
        ivNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewsDetailsActivity.this, ImageActivity.class);
                intent.putExtra(ImageActivity.IMAGE_URL, news.getImageUrl());
                startActivity(intent);
            }
        });
    }

    private void getSingleNews(String newsId) {
        Log.i("getSingleNews", "newsId: " + newsId);
        HttpRequestManager requestManager = new HttpRequestManager();
        final ObjectMapper mapper = new ObjectMapper();

        ResponseHttpHandler handler = new ResponseHttpHandler() {
            @Override
            public void onSuccess(GenericResponseMessage<Object> response) {
                findViewById(R.id.flpbNewsDetails).setVisibility(View.GONE);
                news = mapper.convertValue(response.getPayLoad(), NewsMessage.class);
                fillNews();
            }

            @Override
            public void onFailure(String errorResponse, Throwable e) {
                findViewById(R.id.flpbNewsDetails).setVisibility(View.GONE);
                Log.e("getSingleNews onFailure", e.toString());
                displayAlertDialog(getString(R.string.error_title), errorResponse, null, null);
            }
        };

        try {
            requestManager.asyncHttpRequest(getApplicationContext(), null, String.format(Constant.SINGLE_NEWS_URL, newsId),
                    HttpMethod.GET, handler);
        } catch (Exception e) {
            Log.e("getSingleNews", e.getMessage());
        }
    }

}
