package eu.boss.mobilesport.news;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.externalmodel.NewsListMessage;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.CoteauxApplication;
import eu.boss.mobilesport.main.NavigationDrawerActivity;
import eu.boss.mobilesport.preferences.SettingsActivity;
import eu.boss.mobilesport.util.Constant;

/**
 * This is the main activity displaying news. For now, only the last news are displayed
 */
public class MainActivity extends NavigationDrawerActivity implements AdapterView.OnItemClickListener {

    private static final int ADD_NEWS = 1;

    private NewsListMessage newsList;
    private SwipeRefreshLayout mSwipeRefreshLayout = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayMenuButton = true;
        getNews();

        //Initialize swipe to refresh view
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Refreshing data on server
                getNews();
            }
        });
    }

    private void getNews() {

        HttpRequestManager requestManager = new HttpRequestManager();
        requestManager.asyncHttpRequest(MainActivity.this, null, Constant.NEWS_URL, HttpMethod.GET, new ResponseHttpHandler() {
            @Override
            public void onSuccess(GenericResponseMessage<Object> response) {
                findViewById(R.id.flpbNews).setVisibility(View.GONE);
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    newsList = mapper.convertValue(response.getPayLoad(), NewsListMessage.class);
                    NewsViewAdapter adapter = new NewsViewAdapter(MainActivity.this, newsList);
                    ListView lvNews = (ListView) findViewById(R.id.lvNews);
                    lvNews.setVisibility(View.VISIBLE);
                    lvNews.setAdapter(adapter);
                    lvNews.setOnItemClickListener(MainActivity.this);
                    if (mSwipeRefreshLayout.isRefreshing()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    displayAlertDialog(getString(R.string.error_title), getString(R.string.error_request), null, null);
                }
            }

            @Override
            public void onFailure(String errorResponse, Throwable e) {
                findViewById(R.id.flpbNews).setVisibility(View.GONE);
                if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
                displayAlertDialog(getString(R.string.error_title), errorResponse, null, null);
            }
        });
    }


    public void onItemClick(AdapterView<?> l, View v, int position, long id) {

        // Then you start a new Activity via Intent
        v.setSelected(true);
        Intent intent = new Intent();
        intent.setClass(this, NewsDetailsActivity.class);
        intent.putExtra(NewsDetailsActivity.NEWS, newsList.getNews().get(position));
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (((CoteauxApplication) getApplication()).isLogged())
            getMenuInflater().inflate(R.menu.menu_add_news, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_add_news) {
            Intent intent = new Intent(MainActivity.this, AddNewsActivity.class);
            startActivityForResult(intent, ADD_NEWS);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_NEWS && resultCode == Activity.RESULT_OK) {
            // refresh list after add news
            getNews();
        }
    }

}
