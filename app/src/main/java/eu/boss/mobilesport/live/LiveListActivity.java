package eu.boss.mobilesport.live;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.externalmodel.MatchListMessage;
import eu.boss.library.externalmodel.MatchStatus;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.CoteauxApplication;
import eu.boss.mobilesport.main.NavigationDrawerActivity;
import eu.boss.mobilesport.util.Constant;

public class LiveListActivity extends NavigationDrawerActivity implements AdapterView.OnItemClickListener {

    private MatchListMessage mMatchList;
    private SwipeRefreshLayout mSwipeRefreshLayout = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        displayMenuButton = true;
        setContentView(R.layout.activity_live_list);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshMatchList();
        //Initialize swipe to refresh view
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayoutLive);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Refreshing data on server
                refreshMatchList();
            }
        });
    }

    private void refreshMatchList() {
        HttpRequestManager requestManager = new HttpRequestManager();
        requestManager.asyncHttpRequest(LiveListActivity.this, null, Constant.LIVE_MATCH_URL, HttpMethod.GET, new ResponseHttpHandler() {
            @Override
            public void onSuccess(GenericResponseMessage<Object> response) {
                findViewById(R.id.flpbLive).setVisibility(View.GONE);
                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    mMatchList = mapper.convertValue(response.getPayLoad(), MatchListMessage.class);
                    if (mMatchList.getMatchs().isEmpty()) {
                        findViewById(R.id.empty_live).setVisibility(View.VISIBLE);
                        findViewById(R.id.drawer_layout).setBackgroundColor(getResources().getColor(R.color.bg_light_gray));
                    } else {
                        LiveViewAdapter adapter = new LiveViewAdapter(LiveListActivity.this, mMatchList);
                        ListView lvLive = (ListView) findViewById(R.id.live_list);
                        lvLive.setVisibility(View.VISIBLE);
                        lvLive.setAdapter(adapter);
                        lvLive.setOnItemClickListener(LiveListActivity.this);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    displayAlertDialog(getString(R.string.error_title), getString(R.string.error_request), null, null);
                }
            }

            @Override
            public void onFailure(String errorResponse, Throwable e) {
                findViewById(R.id.flpbLive).setVisibility(View.GONE);
                findViewById(R.id.live_list).setVisibility(View.VISIBLE);
                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
                displayAlertDialog(getString(R.string.error_title), errorResponse, null, null);
            }
        });
    }

    public void onItemClick(AdapterView<?> l, View v, int position, long id) {

        // Then you start a new Activity via Intent
        if ((mMatchList.getMatchs().get(position).getStatus() != MatchStatus.NOT_STARTED) || (((CoteauxApplication) getApplication()).isLogged())) {
            v.setSelected(true);
            Intent intent = new Intent();
            intent.setClass(this, LiveDetailsActivity.class);
            intent.putExtra("match", mMatchList.getMatchs().get(position));
            startActivity(intent);
        }
    }

}
