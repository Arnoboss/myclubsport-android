package eu.boss.mobilesport.live;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import eu.boss.mobilesport.R;
import eu.boss.mobilesport.util.Constant;

/**
 * Fragment containing a simple Webview. Display a basic html page from URL.
 */
public class WebViewFragmentV4 extends android.support.v4.app.Fragment {

    private WebView mWebView;
    private String mUrl;
    private ProgressBar mProgressBar;

    public WebViewFragmentV4() {
        // Required empty public constructor
    }

    public static WebViewFragmentV4 newInstance(String url) {
        WebViewFragmentV4 fragment = new WebViewFragmentV4();
        Bundle args = new Bundle();
        args.putString(Constant.URL, url);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUrl = getArguments().getString(Constant.URL);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_web_view, container, false);
        // Inflate the layout for this fragment
        mWebView = (WebView) mainView.findViewById(R.id.webViewFragment);
        mProgressBar = (ProgressBar) mainView.findViewById(R.id.progressBar);

        return mainView;

    }

    @Override
    public void onStart() {
        super.onStart();
        mWebView.setBackgroundColor(Color.WHITE);
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            mWebView.setLayerType(
                    WebView.LAYER_TYPE_SOFTWARE, null);
            mWebView.getSettings().setDisplayZoomControls(false);
        }
        mWebView.getSettings().setAppCacheEnabled(false);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.setScrollbarFadingEnabled(false);
        mWebView.getSettings().setSupportZoom(false);
        mWebView.setInitialScale(95);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                mProgressBar.setVisibility(View.GONE);
            }
        });

        mWebView.loadUrl(mUrl);
    }

}
