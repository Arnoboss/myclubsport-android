package eu.boss.mobilesport.live;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.boss.library.externalmodel.CommentMessage;
import eu.boss.library.externalmodel.GenericAuthRequestMessage;
import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.externalmodel.MatchMessage;
import eu.boss.library.externalmodel.MatchStatus;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.library.util.PrefsManager;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.CoteauxApplication;
import eu.boss.mobilesport.main.NavigationDrawerActivity;
import eu.boss.mobilesport.util.Constant;

public class AddNewCommentActivity extends NavigationDrawerActivity {
    public static final String EDIT_COMMENT = "commentToEdit";
    public static final String MATCH = "match";
    View.OnClickListener clickListenerButeur = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };
    View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            v.setFocusable(true);
            v.setFocusableInTouchMode(true);
            v.requestFocus();
            ((EditText) v).setSelection(((EditText) v).getText().length());
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);

            return true;
        }
    };
    private EditText etScoreTeam1;
    private EditText etScoreTeam2;
    private EditText etButeurTeam1;
    private EditText etButeurTeam2;
    private EditText etMinute;
    private EditText etComment;
    private CheckBox cbImportant;
    private CheckBox cbNotify;
    private Button btnConfirm;
    private MatchMessage match;
    private Spinner mSpinner;
    private CommentMessage commentToEdit;
    private ProgressDialog progressDialog;
    //Key= Scorer name; Value = Number of goals
    private LinkedHashMap<String, Integer> scorersMapTeam1 = new LinkedHashMap<>();
    private LinkedHashMap<String, Integer> scorersMapTeam2 = new LinkedHashMap<>();
    private boolean isEditing = false;
    private TextView tvTeam1;
    private TextView tvTeam2;
    View.OnClickListener clickListenerScore = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(AddNewCommentActivity.this, AddGoalActivity.class);
            intent.putExtra(Constant.MATCH_ID, match.getId());
            if (v.getId() == R.id.etLiveAddScoreTeam1) {
                //increments team1
                intent.putExtra(Constant.TEAM, tvTeam1.getText());
                startActivityForResult(intent, Constant.GOAL_TEAM_1);

            } else if (v.getId() == R.id.etLiveAddScoreTeam2) {
                //increments team2
                intent.putExtra(Constant.TEAM, tvTeam2.getText());
                startActivityForResult(intent, Constant.GOAL_TEAM_2);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_news_comment);

        match = (MatchMessage) getIntent().getExtras().get(MATCH);

        tvTeam1 = ((TextView) findViewById(R.id.tvLiveAddTeam1));
        tvTeam2 = ((TextView) findViewById(R.id.tvLiveAddTeam2));
        etScoreTeam1 = (EditText) findViewById(R.id.etLiveAddScoreTeam1);
        etScoreTeam2 = (EditText) findViewById(R.id.etLiveAddScoreTeam2);
        etButeurTeam1 = (EditText) findViewById(R.id.etGoalsAddTeam1);
        etButeurTeam2 = (EditText) findViewById(R.id.etGoalsAddTeam2);
        etMinute = (EditText) findViewById(R.id.etAddDetailsMinute);
        etComment = (EditText) findViewById(R.id.etAddDetailsComment);
        cbImportant = (CheckBox) findViewById(R.id.checkBoxBold);
        cbNotify = (CheckBox) findViewById(R.id.checkBoxNotify);
        btnConfirm = (Button) findViewById(R.id.btnConfirmComment);
        mSpinner = (Spinner) findViewById(R.id.spinnerMatchState);

        tvTeam1.setText(match.getTeam1());
        tvTeam2.setText(match.getTeam2());
        etScoreTeam1.setText("" + match.getGoalsTeam1());
        etScoreTeam2.setText("" + match.getGoalsTeam2());
        etButeurTeam1.setText(match.getButeursTeam1());
        etButeurTeam2.setText(match.getButeursTeam2());


        if (match.getStatus() == MatchStatus.NOT_STARTED) {
            mSpinner.setSelection(0);
        } else if (match.getStatus() == MatchStatus.IN_PROGRESS) {
            mSpinner.setSelection(1);
        } else if (match.getStatus() == MatchStatus.FINISHED) {
            mSpinner.setSelection(2);
        }

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayAlertDialog(getString(R.string.comment_confirm_title), getString(R.string.comment_confirm), getString(R.string.confirm), getString(R.string.cancel));
            }
        });

        if (getIntent().getExtras().get(EDIT_COMMENT) != null) {
            isEditing = true;
            commentToEdit = (CommentMessage) getIntent().getExtras().get(EDIT_COMMENT);
            etMinute.setText("" + commentToEdit.getMinuteEvent());
            etComment.setText(commentToEdit.getContent());
            cbImportant.setChecked(commentToEdit.isImportant());
            setTitle(getString(R.string.editComment));
        }

        initScorersHashmaps();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (((CoteauxApplication) getApplication()).getPlayersForMatch(match.getId()) == null) {
            getPlayersList(match.getId());
        } else if (!((CoteauxApplication) getApplication()).getPlayersForMatch(match.getId()).isEmpty())
            initEditTexts(false);
    }

    /**
     * Initialization of editTexts. If there are no players in the list, scores and scorers are still editable only manually.
     */
    protected void initEditTexts(boolean isPlayerListEmpty) {
        if (!isPlayerListEmpty) {
            etButeurTeam1.setFocusable(false);
            etButeurTeam1.setClickable(true);
            etButeurTeam2.setFocusable(false);
            etButeurTeam2.setClickable(true);
            etScoreTeam1.setFocusable(false);
            etScoreTeam1.setClickable(true);
            etScoreTeam2.setFocusable(false);
            etScoreTeam2.setClickable(true);
            etButeurTeam1.setOnClickListener(clickListenerButeur);
            etButeurTeam2.setOnClickListener(clickListenerButeur);
            etButeurTeam1.setOnLongClickListener(longClickListener);
            etButeurTeam2.setOnLongClickListener(longClickListener);
            etScoreTeam1.setOnClickListener(clickListenerScore);
            etScoreTeam2.setOnClickListener(clickListenerScore);
            etScoreTeam1.setOnLongClickListener(longClickListener);
            etScoreTeam2.setOnLongClickListener(longClickListener);
        }
    }

    /**
     * Convertit les listes des buteurs en hashmaps
     */
    private void initScorersHashmaps() {
        Pattern p = Pattern.compile("(([a-zA-Z. ]+)(\\s[x])([0-9]+))");
        if (match.getButeursTeam1() != null) {
            String[] separatedScorers1 = match.getButeursTeam1().split(",");

            for (int i = 0; i < separatedScorers1.length; i++) {
                Matcher m = p.matcher(separatedScorers1[i]);
                if (m.matches()) {
                    //Le joueur a marqué plusieurs buts
                    scorersMapTeam1.put(m.group(2).trim(), Integer.valueOf(m.group(4)));
                } else
                    scorersMapTeam1.put(separatedScorers1[i].trim(), 1);
            }
        }
        if (match.getButeursTeam2() != null) {
            String[] separatedScorers2 = match.getButeursTeam2().split(",");
            for (int i = 0; i < separatedScorers2.length; i++) {
                Matcher m = p.matcher(separatedScorers2[i]);
                if (m.matches()) {
                    //Le joueur a marqué plusieurs buts
                    scorersMapTeam2.put(m.group(2).trim(), Integer.valueOf(m.group(4)));
                } else
                    scorersMapTeam2.put(separatedScorers2[i].trim(), 1);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        // Check which request we're responding to
        if (resultCode == RESULT_OK) {
            if (requestCode == Constant.GOAL_TEAM_1) {
                // Make sure the request was successful -> increments score
                int oldScore = Integer.valueOf(etScoreTeam1.getText().toString());
                etScoreTeam1.setText("" + ++oldScore);
                if ((intent.getExtras() != null) && (intent.getExtras().getString(Constant.SCORER) != null)) {
                    addGoalForPlayer(scorersMapTeam1, intent.getExtras().getString(Constant.SCORER));
                }
            } else if (requestCode == Constant.GOAL_TEAM_2) {
                int oldScore = Integer.valueOf(etScoreTeam2.getText().toString());
                etScoreTeam2.setText("" + ++oldScore);
                if ((intent.getExtras() != null) && (intent.getExtras().getString(Constant.SCORER) != null)) {
                    addGoalForPlayer(scorersMapTeam2, intent.getExtras().getString(Constant.SCORER));
                }
            }
            updateScorersList();
        }
    }

    //Add goal for players in the hashmap
    private void addGoalForPlayer(LinkedHashMap<String, Integer> scorersMap, String playerName) {
        Integer numberOfGoals = 1;
        if (scorersMap.containsKey(playerName))
            numberOfGoals = scorersMap.get(playerName) + 1;
        scorersMap.put(playerName, numberOfGoals);
    }

    /**
     * Update the scorers list iterating through hashmaps
     */
    private void updateScorersList() {
        etButeurTeam1.setText("");
        for (Map.Entry<String, Integer> entry : scorersMapTeam1.entrySet()) {
            String oldList = etButeurTeam1.getText().toString();
            if (!oldList.equals("")) oldList += ", ";
            String name = entry.getKey();
            Integer value = entry.getValue();
            if (value > Integer.valueOf(1))
                etButeurTeam1.setText(oldList + name + " x" + value);
            else
                etButeurTeam1.setText(oldList + name);
        }

        etButeurTeam2.setText("");
        for (Map.Entry<String, Integer> entry : scorersMapTeam2.entrySet()) {
            String oldList = etButeurTeam2.getText().toString();
            if (!oldList.equals("")) oldList += ", ";
            String name = entry.getKey();
            Integer value = entry.getValue();
            if (value > Integer.valueOf(1))
                etButeurTeam2.setText(oldList + name + " x" + value);
            else
                etButeurTeam2.setText(oldList + name);
        }
    }

    @Override
    protected void clickOnPositiveDialog() {

        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(AddNewCommentActivity.this, ProgressDialog.STYLE_SPINNER);
                progressDialog.setMessage(getString(R.string.loading));
                progressDialog.setCanceledOnTouchOutside(false);
            }
            progressDialog.show();
            postNewComment(mapMatchMessage());
        } catch (Exception e) {
            Log.e("error sending comment", e.toString());
        }
    }

    @Override
    protected void clickOnNegativeDialog() {
        alertDialog.cancel();
    }


    private MatchMessage mapMatchMessage() throws Exception {
        String errorMsg = "";
        match.setButeursTeam1(etButeurTeam1.getText().toString());
        match.setButeursTeam2(etButeurTeam2.getText().toString());

        int matchStatusPosition = mSpinner.getSelectedItemPosition();
        String[] matchStatus_values = getResources().getStringArray(R.array.match_status_values);
        match.setStatus(MatchStatus.valueOf(matchStatus_values[matchStatusPosition]));

        try {
            if (etScoreTeam1.getText().toString().equals("") || etScoreTeam2.getText().toString().equals(""))
                throw new Exception();
            match.setGoalsTeam1(Integer.parseInt(etScoreTeam1.getText().toString()));
            match.setGoalsTeam2(Integer.parseInt(etScoreTeam2.getText().toString()));
        } catch (Exception e) {
            errorMsg = getString(R.string.error_comment_score_value);
        }

        if (etComment.getText().toString() == null || etComment.getText().toString().trim().equals("")) {
            errorMsg += " " + getString(R.string.error_comment_empty);
        }
        CommentMessage commentMessage = new CommentMessage();
        commentMessage.setContent(etComment.getText().toString());
        commentMessage.setImportant(cbImportant.isChecked());
        commentMessage.setNotify(cbNotify.isChecked());

        //On spécifie l'ID si on est en train d'éditer le commentaire. Cela sert pour le matching coté serveur
        if (isEditing)
            commentMessage.setId(commentToEdit.getId());

        try {
            commentMessage.setMinuteEvent(Integer.parseInt(etMinute.getText().toString()));
        } catch (Exception e) {
            errorMsg += " " + getString(R.string.error_comment_minute_value);
        }

        ArrayList<CommentMessage> commentList = new ArrayList<>();
        commentList.add(commentMessage);
        match.setComments(commentList);

        if (!errorMsg.equals("")) {
            displayAlertDialog(getString(R.string.error_title), errorMsg, null, null);
            throw new Exception("errorMsg");
        }
        return match;
    }

    @Override
    public void displayAlertDialog(String title, String message, String labelBtn1, String labelBtn2) {

        try {
            if (progressDialog != null)
                progressDialog.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle(title)
                    .setMessage(message);
            if (labelBtn2 == null)
                builder.setPositiveButton(R.string.ok, null);
            else {

                builder.setPositiveButton(labelBtn1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        clickOnPositiveDialog();
                    }
                })
                        .setNegativeButton(labelBtn2, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                clickOnNegativeDialog();
                            }
                        });

            }
            builder.setIcon(android.R.drawable.ic_dialog_alert);

            alertDialog = builder.show();
        } catch (WindowManager.BadTokenException e) {
            //nothing to do: the activity is not running anymore
        }
    }

    private void postNewComment(MatchMessage matchMessage) throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        try {
            GenericAuthRequestMessage<MatchMessage> requestMsg = new GenericAuthRequestMessage<>();
            requestMsg.setPayLoad(matchMessage);
            requestMsg.setTokenId(PrefsManager.loadString(AddNewCommentActivity.this, Constant.TOKEN_ID));
            requestMsg.setUserId(PrefsManager.loadString(AddNewCommentActivity.this, Constant.LOGIN));

            HttpRequestManager requestManager = new HttpRequestManager();

            ResponseHttpHandler handler = new ResponseHttpHandler() {
                @Override
                public void onSuccess(GenericResponseMessage<Object> response) {

                    try {
                        if (progressDialog != null)
                            progressDialog.dismiss();
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                        displayAlertDialog(getString(R.string.error_title), getString(R.string.error_request), null, null);
                    }
                }

                @Override
                public void onFailure(String errorResponse, Throwable e) {
                    if (progressDialog != null)
                        progressDialog.dismiss();
                    displayAlertDialog(getString(R.string.error_title), errorResponse, null, null);
                }
            };

            if (isEditing)
                requestManager.asyncHttpRequest(AddNewCommentActivity.this, mapper.writeValueAsString(requestMsg), Constant.LIVE_MATCH_COMMENTS_URL, HttpMethod.PUT, true, handler);
            else
                requestManager.asyncHttpRequest(AddNewCommentActivity.this, mapper.writeValueAsString(requestMsg), Constant.LIVE_MATCH_COMMENTS_URL, HttpMethod.POST, true, handler);
        } catch (Exception e) {
            Log.e("Error send comment", e.toString());
        }
    }

    /**
     * Get player list from server or from service. We store the list in the service to make less server request and make the app faster
     *
     * @param matchId match id
     */
    private void getPlayersList(final Long matchId) {

        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(AddNewCommentActivity.this, ProgressDialog.STYLE_SPINNER);
                progressDialog.setMessage(getString(R.string.loading));
                progressDialog.setCanceledOnTouchOutside(false);
            }
            progressDialog.show();

            HttpRequestManager requestManager = new HttpRequestManager();

            ResponseHttpHandler handler = new ResponseHttpHandler() {
                @Override
                public void onSuccess(GenericResponseMessage<Object> response) {
                    ObjectMapper mapper = new ObjectMapper();
                    try {
                        if (progressDialog != null)
                            progressDialog.dismiss();
                        List<String> playerList = mapper.convertValue(response.getPayLoad(), List.class);
                        ((CoteauxApplication) getApplication()).setPlayersForMatch(playerList, matchId);
                        initEditTexts(playerList.isEmpty());
                    } catch (Exception e) {
                        e.printStackTrace();
                        displayAlertDialog(getString(R.string.error_title), getString(R.string.error_request), null, null);
                    }
                }

                @Override
                public void onFailure(String errorResponse, Throwable e) {
                    if (progressDialog != null)
                        progressDialog.dismiss();
                    displayAlertDialog(getString(R.string.error_title), errorResponse, null, null);
                }
            };

            requestManager.asyncHttpRequest(AddNewCommentActivity.this, null, String.format(Constant.PLAYERS_FOR_MATCH_URL, matchId), HttpMethod.GET, true, handler);
        } catch (Exception e) {
            Log.e("Error send comment", e.toString());
        }
    }
}
