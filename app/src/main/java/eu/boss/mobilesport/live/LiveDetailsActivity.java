package eu.boss.mobilesport.live;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import eu.boss.library.externalmodel.Category;
import eu.boss.library.externalmodel.MatchMessage;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.CoteauxApplication;
import eu.boss.mobilesport.main.NavigationDrawerActivity;
import eu.boss.mobilesport.util.Constant;
import eu.boss.mobilesport.views.FragmentTabHostCustom;

public class LiveDetailsActivity extends NavigationDrawerActivity {

    MatchMessage mMatch;
    private FragmentTabHostCustom mTabHost = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_details);

        if (getIntent().getSerializableExtra("match") != null) {
            mMatch = (MatchMessage) getIntent().getSerializableExtra("match");
            updateHeader();
        } else if (getIntent().getSerializableExtra(Constant.DETAILS_ID) != null) {
            mMatch = new MatchMessage();
            mMatch.setId(Long.valueOf(getIntent().getStringExtra(Constant.DETAILS_ID)));
        }

        mTabHost = (FragmentTabHostCustom) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        //On utilise les bundle pour passer des parametres aux fragments
        Bundle b1 = new Bundle();
        Bundle b2 = new Bundle();
        b1.putSerializable(Constant.MATCH, mMatch);
        b2.putString(Constant.URL, Constant.FORMATION_VISU_URL + mMatch.getId());
        mTabHost.addTab(mTabHost.newTabSpec("0").setIndicator(getString(R.string.comments)), CommentsFragment.class, b1);
        mTabHost.addTab(mTabHost.newTabSpec("1").setIndicator(getString(R.string.composition)), WebViewFragmentV4.class, b2);
        //Resize tab height
        mTabHost.getTabWidget().getChildAt(0).getLayoutParams().height = (int) (40 * this.getResources().getDisplayMetrics().density);
        mTabHost.getTabWidget().getChildAt(1).getLayoutParams().height = (int) (40 * this.getResources().getDisplayMetrics().density);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTabHost = null;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (((CoteauxApplication) getApplication()).isLogged())//&& mMatch.getStatus()!= MatchStatus.FINISHED)
            getMenuInflater().inflate(R.menu.menu_live, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add_comment) {
            Intent intent = new Intent(LiveDetailsActivity.this, AddNewCommentActivity.class);
            intent.putExtra(AddNewCommentActivity.MATCH, mMatch);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateHeader() {
        ((TextView) findViewById(R.id.tvLiveDetailsTeam1)).setText(mMatch.getTeam1());
        ((TextView) findViewById(R.id.tvLiveDetailsTeam2)).setText(mMatch.getTeam2());
        String team = Category.valueOf(mMatch.getCategoryName()).getName() + " " + mMatch.getTeamName();
        ((TextView) findViewById(R.id.tvLiveDetailsDate)).setText(team + " - " + mMatch.getFormatedDateComplete());
    }

    public void updateUI(MatchMessage match) {
        this.mMatch = match;
        updateHeader();
        findViewById(R.id.llLiveDetails).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.tvLiveDetailsScore)).setText(mMatch.getGoalsTeam1() + " - " + mMatch.getGoalsTeam2());
        ((TextView) findViewById(R.id.tvGoalsTeam1)).setText(mMatch.getButeursTeam1());
        ((TextView) findViewById(R.id.tvGoalsTeam2)).setText(mMatch.getButeursTeam2());
    }

}
