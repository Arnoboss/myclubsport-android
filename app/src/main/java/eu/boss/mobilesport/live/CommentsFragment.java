package eu.boss.mobilesport.live;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.externalmodel.MatchMessage;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.CoteauxApplication;
import eu.boss.mobilesport.main.NavigationDrawerActivity;
import eu.boss.mobilesport.util.Constant;

/**
 * A fragment representing a list of comments
 */
public class CommentsFragment extends android.support.v4.app.Fragment implements AdapterView.OnItemClickListener {

    private MatchMessage mMatch;
    private SwipeRefreshLayout mSwipeRefreshLayout = null;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CommentsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mMatch = (MatchMessage) getArguments().getSerializable(Constant.MATCH);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_comments, container, false);
        //Initialize swipe to refresh view
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayoutLiveDetails);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Refreshing data on server
                getComments();
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        getComments();
    }

    @Override
    public void onItemClick(AdapterView<?> l, View v, int position, long id) {
        Intent intent = new Intent(getActivity(), AddNewCommentActivity.class);
        intent.putExtra(AddNewCommentActivity.MATCH, mMatch);
        intent.putExtra(AddNewCommentActivity.EDIT_COMMENT, new ArrayList<>(mMatch.getComments()).get(position));
        startActivity(intent);
    }


    private void getComments() {
        HttpRequestManager requestManager = new HttpRequestManager();
        requestManager.asyncHttpRequest(getActivity(), null, Constant.LIVE_MATCH_COMMENTS_URL + "?matchId=" + mMatch.getId() + "&includeCatAndTeam=true", HttpMethod.GET, new ResponseHttpHandler() {
            @Override
            public void onSuccess(GenericResponseMessage<Object> response) {
                getActivity().findViewById(R.id.flpbComments).setVisibility(View.GONE);
                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    mMatch = mapper.convertValue(response.getPayLoad(), MatchMessage.class);
                    if (mMatch != null) {
                        ((LiveDetailsActivity) getActivity()).updateUI(mMatch);

                        //la cellule est cliquable que si on est loggé
                        boolean clickableCell = ((CoteauxApplication) getActivity().getApplication()).isLogged();
                        LiveViewDetailsAdapter adapter = new LiveViewDetailsAdapter(getActivity(), mMatch.getComments(), clickableCell);
                        ListView lvLive = (ListView) getView().findViewById(R.id.lvLiveComments);
                        lvLive.setVisibility(View.VISIBLE);
                        lvLive.setAdapter(adapter);
                        if (clickableCell) {
                            lvLive.setOnItemClickListener(CommentsFragment.this);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ((NavigationDrawerActivity) getActivity()).displayAlertDialog(getString(R.string.error_title), getString(R.string.error_request), null, null);
                }
            }

            @Override
            public void onFailure(String errorResponse, Throwable e) {
                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
                getActivity().findViewById(R.id.flpbComments).setVisibility(View.GONE);
                ((NavigationDrawerActivity) getActivity()).displayAlertDialog(getString(R.string.error_title), errorResponse, null, null);
            }
        });
    }

}
