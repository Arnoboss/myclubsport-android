package eu.boss.mobilesport.live;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.List;

import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.CoteauxApplication;
import eu.boss.mobilesport.util.Constant;

public class AddGoalActivity extends AppCompatActivity {

    private RadioGroup radioGroup;

    private List<String> playersList;


    @Override
    protected void onStart() {
        super.onStart();
        initRadioButtons();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_goal);
    }

    private void initRadioButtons() {
        String teamName = getIntent().getExtras().getString(Constant.TEAM);
        Long matchId = getIntent().getExtras().getLong(Constant.MATCH_ID);
        setTitle(getTitle() + " " + teamName);
        Button btnConfirm = (Button) findViewById(R.id.btnConfirmGoal);
        Button btnCancel = (Button) findViewById(R.id.btnCancelGoal);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroupGoal);

        // On teste le nom de l'équipe sélectionnée. Si c'est les coteaux, on va chercher la liste des joueurs du match.
        // C'est pas très beau, mais pour que ça reste compatible avec les vieilles versions on n'a pas le choix
        if ((teamName.toLowerCase().contains(getString(R.string.team_name))) || (teamName.toLowerCase().contains(getString(R.string.team_name2)))) {
            playersList = ((CoteauxApplication) getApplication()).getPlayersForMatch(matchId);
            addRadioButtons(true);
        } else {
            addRadioButtons(false);
        }

        View.OnClickListener onClickListener = new View.OnClickListener() {

            public void onClick(View v) {
                if (v.getId() == R.id.btnConfirmGoal) {
                    Intent returnIntent = new Intent();

                    //Get selected radio button
                    int radioButtonID = radioGroup.getCheckedRadioButtonId();
                    View radioButton = radioGroup.findViewById(radioButtonID);
                    int idx = radioGroup.indexOfChild(radioButton);
                    RadioButton r = (RadioButton) radioGroup.getChildAt(idx);
                    String selectedScorer = r.getText().toString();
                    // On n'affiche pas le nom "adversaire", c'est inutile
                    if (!selectedScorer.equals(getString(R.string.opponent)))
                        returnIntent.putExtra(Constant.SCORER, selectedScorer);
                    setResult(Activity.RESULT_OK, returnIntent);
                }
                finish();
            }
        };
        btnConfirm.setOnClickListener(onClickListener);
        btnCancel.setOnClickListener(onClickListener);
    }

    private void addRadioButtons(boolean addPlayers) {
        RadioGroup.LayoutParams rprms;
        if (addPlayers) {
            for (int i = 0; i < playersList.size(); i++) {
                RadioButton radioButton = new RadioButton(this);
                radioButton.setText(playersList.get(i));
                radioButton.setId(i);
                rprms = new RadioGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                radioGroup.addView(radioButton, rprms);
            }
        } else {
            RadioButton radioButtonADV = new RadioButton(this);
            radioButtonADV.setText(getString(R.string.opponent));
            radioButtonADV.setId(R.id.opponent);
            rprms = new RadioGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            radioGroup.addView(radioButtonADV, rprms);
        }
        RadioButton radioButtonCSC = new RadioButton(this);
        radioButtonCSC.setText(getString(R.string.csc));
        radioButtonCSC.setId(R.id.csc);
        rprms = new RadioGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        radioGroup.addView(radioButtonCSC, rprms);
    }

}
