package eu.boss.mobilesport.live;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import eu.boss.library.externalmodel.MatchListMessage;
import eu.boss.library.externalmodel.MatchMessage;
import eu.boss.library.externalmodel.MatchStatus;
import eu.boss.mobilesport.R;

/**
 * Created by Arnaud on 20/07/2015.
 */
public class LiveViewAdapter extends BaseAdapter {


    private MatchListMessage mMatchList;
    private LayoutInflater inflater;
    private Context mContext;

    public LiveViewAdapter(Context context, MatchListMessage matchList
    ) {
        inflater = LayoutInflater.from(context);
        mContext = context;
        mMatchList = matchList;
    }


    @Override
    public int getCount() {
        return mMatchList.getMatchs().size();
    }

    @Override
    public Object getItem(int position) {
        return mMatchList.getMatchs().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        MatchMessage match = mMatchList.getMatchs().get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.live_item, null);
            holder.tvTeam1 = (TextView) convertView.findViewById(R.id.tvLiveTeam1);
            holder.tvTeam2 = (TextView) convertView.findViewById(R.id.tvLiveTeam2);
            holder.tvScore = (TextView) convertView.findViewById(R.id.tvLiveScore);
            holder.tvDate = (TextView) convertView.findViewById(R.id.tvLiveDate);
            holder.tvStatus = (TextView) convertView.findViewById(R.id.tvLiveMatchStatus);
            holder.tvIntitule = (TextView) convertView.findViewById(R.id.tvLiveIntitule);
            //holder.tvLiveTeamCat = (TextView) convertView.findViewById(R.id.tvLiveTeamCat);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        holder.tvTeam1.setText(match.getTeam1());
        holder.tvTeam2.setText(match.getTeam2());
        holder.tvDate.setText(match.getFormatedDate());
        holder.tvIntitule.setText(match.getIntitule());
        // holder.tvLiveTeamCat.setText(Category.valueOf(match.getCategoryName()).getName() + " " + match.getTeamName());
        if ((match.getStatus() == MatchStatus.FINISHED) || (match.getStatus() == MatchStatus.IN_PROGRESS)) {
            holder.tvStatus.setText(match.getMatchStatusStr(mContext));
            holder.tvScore.setText(match.getGoalsTeam1() + " - " + match.getGoalsTeam2());
        } else {
            holder.tvStatus.setVisibility(View.GONE);
            holder.tvScore.setText(match.getMatchTime());
        }

        return convertView;
    }

    public void addAll(MatchListMessage list) {
        this.mMatchList.getMatchs().addAll(list.getMatchs());
        notifyDataSetChanged();
    }

    public void clear() {
        mMatchList.getMatchs().clear();
    }

    public void replace(MatchListMessage list) {
        clear();
        addAll(list);
    }

    public MatchMessage getMatch(int position) {
        return mMatchList.getMatchs().get(position);
    }


    private class ViewHolder {

        TextView tvDate;
        TextView tvTeam1;
        TextView tvTeam2;
        TextView tvScore;
        TextView tvStatus;
        TextView tvIntitule;
        //TextView tvLiveTeamCat;
    }
}
