package eu.boss.mobilesport.live;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import eu.boss.library.externalmodel.CommentMessage;
import eu.boss.mobilesport.R;

/**
 * Adapteur servant a remplir la listview des commentaires
 * Created by Arnaud on 20/07/2015.
 */
public class LiveViewDetailsAdapter extends BaseAdapter {


    private List<CommentMessage> mCommentsList;
    private LayoutInflater inflater;
    private boolean mClickableCell;

    public LiveViewDetailsAdapter(Context context, Collection<CommentMessage> commentsList, boolean areClickableCells
    ) {
        inflater = LayoutInflater.from(context);
        mCommentsList = new ArrayList<>(commentsList);
        mClickableCell = areClickableCells;
    }


    @Override
    public int getCount() {
        return mCommentsList.size();
    }

    @Override
    public Object getItem(int position) {
        return mCommentsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        CommentMessage comment = mCommentsList.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.live_item_details, null);
            holder.tvMinute = (TextView) convertView.findViewById(R.id.tvLiveDetailsMinute);
            holder.tvComment = (TextView) convertView.findViewById(R.id.tvLiveDetailsComment);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        //On met en gras les commentaires importants
        if (comment.isImportant()) {
            holder.tvMinute.setTypeface(null, Typeface.BOLD);
            holder.tvComment.setTypeface(null, Typeface.BOLD);
        } else {
            holder.tvMinute.setTypeface(null, Typeface.NORMAL);
            holder.tvComment.setTypeface(null, Typeface.NORMAL);
        }
        holder.tvMinute.setText(comment.getMinuteEvent() + "'");
        holder.tvComment.setText(comment.getContent());

        return convertView;
    }

    public void addAll(List<CommentMessage> list) {
        this.mCommentsList.addAll(list);
        notifyDataSetChanged();
    }

    public void clear() {
        mCommentsList.clear();
    }

    public void replace(List<CommentMessage> list) {
        clear();
        addAll(list);
    }

    public CommentMessage getComment(int position) {
        return mCommentsList.get(position);
    }

    @Override
    public boolean isEnabled(int position) {
        return mClickableCell;
    }

    private class ViewHolder {

        TextView tvMinute;
        TextView tvComment;
    }
}
