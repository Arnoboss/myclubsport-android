package eu.boss.mobilesport.fffactivities;


import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import eu.boss.library.externalmodel.Category;
import eu.boss.library.externalmodel.TeamNameMessage;


/**
 * Created by Arnaud on 08/05/2015.
 */
public class ViewPagerWebViewAdapter extends FragmentPagerAdapter {

    // Tab Titles
    private String tabTitles[];
    private WebViewFragment[] fragments;
    private String mUrl;
    private int numberOfTeamInCategory;
    private Category category;

    /**
     * @param fm                     fragment manager
     * @param numberOfTeamInCategory Number of pages in pager
     * @param url                    base url to call
     */
    public ViewPagerWebViewAdapter(FragmentManager fm, Category cat, int numberOfTeamInCategory, String url) {
        super(fm);
        this.mUrl = url;
        this.numberOfTeamInCategory = numberOfTeamInCategory;
        this.tabTitles = new String[numberOfTeamInCategory];
        this.fragments = new WebViewFragment[numberOfTeamInCategory];
        this.category = cat;
        if (numberOfTeamInCategory == 1)
            tabTitles[0] = category.getName();
        else {
            for (int i = 0; i < numberOfTeamInCategory; i++) {
                tabTitles[i] = category.getName() + " " + TeamNameMessage.values()[i].name();
            }
        }

    }

    @Override
    public int getCount() {
        return numberOfTeamInCategory;
    }

    @Override
    public Fragment getItem(int position) {
        if (fragments[position] == null)
            fragments[position] = WebViewFragment.newInstance(String.format(mUrl, category.name(), TeamNameMessage.values()[position].name()));
        return fragments[position];
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
