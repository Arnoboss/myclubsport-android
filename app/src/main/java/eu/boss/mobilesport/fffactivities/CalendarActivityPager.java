package eu.boss.mobilesport.fffactivities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import eu.boss.library.externalmodel.Category;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.CoteauxApplication;
import eu.boss.mobilesport.main.ViewPagerTabActivity;
import eu.boss.mobilesport.util.Constant;

public class CalendarActivityPager extends ViewPagerTabActivity {

    private Category category;
    private int numberOfTeamInCategory = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        displayMenuButton = true;
    }

    @Override
    protected void initViewPager() {
        // Set the ViewPagerAdapter into V
        category = (Category) getIntent().getExtras().getSerializable(Constant.CATEGORY);
        numberOfTeamInCategory = ((CoteauxApplication) getApplication()).getNumberOfTeamInCategory(category);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new ViewPagerWebViewAdapter(getFragmentManager(), category, numberOfTeamInCategory, Constant.CALENDAR_URL));
    }
}
