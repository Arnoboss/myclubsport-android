package eu.boss.mobilesport.fffactivities;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.externalmodel.HtmlMessage;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.NavigationDrawerActivity;

/**
 * Base activity displaying webview
 * <p/>
 * Created by Arnaud on 08/05/2015.
 */
public abstract class WebViewActivity extends NavigationDrawerActivity {

    protected WebView mWebView;
    protected String mUrl;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        mWebView = (WebView) findViewById(R.id.webView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar2);
    }

    @Override
    protected void onStart() {
        super.onStart();
        performTask();
    }

    private void performTask() {

        HttpRequestManager requestManager = new HttpRequestManager();
        requestManager.asyncHttpRequest(WebViewActivity.this, null, mUrl, HttpMethod.GET, new ResponseHttpHandler() {
            @Override
            public void onSuccess(GenericResponseMessage<Object> response) {
                mProgressBar.setVisibility(View.GONE);
                ObjectMapper mapper = new ObjectMapper();
                HtmlMessage htmlMessage = mapper.convertValue(response.getPayLoad(), HtmlMessage.class);
                updateWebView(htmlMessage.getHtmlContent());
            }

            @Override
            public void onFailure(String errorResponse, Throwable e) {
                mProgressBar.setVisibility(View.GONE);
                displayAlertDialog(getString(R.string.error_title), errorResponse, null, null);
            }
        });
    }

    public void updateWebView(String result) {

        mWebView.setBackgroundColor(Color.WHITE);
        if (android.os.Build.VERSION.SDK_INT >= 11) mWebView.setLayerType(
                WebView.LAYER_TYPE_SOFTWARE, null);

        mWebView.loadDataWithBaseURL("file:///android_asset/", result, "text/html", "utf-8", null);
    }

}
