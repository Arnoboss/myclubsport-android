package eu.boss.mobilesport.fffactivities;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.externalmodel.HtmlMessage;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.NavigationDrawerActivity;
import eu.boss.mobilesport.util.Constant;

/**
 * Fragment containing a simple webView.
 * Data are loaded from HTML code obtained by the server
 */
public class WebViewFragment extends Fragment {

    private WebView mWebView;
    private String mUrl;
    private ProgressBar mProgressBar;

    public WebViewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param url     url to load
     * @return A new instance of fragment WebViewFragment.
     */
    public static WebViewFragment newInstance(String url) {
        WebViewFragment fragment = new WebViewFragment();
        Bundle args = new Bundle();
        args.putString(Constant.URL, url);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUrl = getArguments().getString(Constant.URL);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_web_view, container, false);
        // Inflate the layout for this fragment
        mWebView = (WebView) mainView.findViewById(R.id.webViewFragment);
        mProgressBar = (ProgressBar) mainView.findViewById(R.id.progressBar);

        return mainView;

    }

    @Override
    public void onStart() {
        super.onStart();
        performTask();
    }

    private void performTask() {

        HttpRequestManager requestManager = new HttpRequestManager();

        requestManager.asyncHttpRequest(getActivity(), null, mUrl, HttpMethod.GET, new ResponseHttpHandler() {
            @Override
            public void onSuccess(GenericResponseMessage<Object> response) {
                mProgressBar.setVisibility(View.GONE);
                ObjectMapper mapper = new ObjectMapper();
                HtmlMessage htmlMessage = mapper.convertValue(response.getPayLoad(), HtmlMessage.class);
                updateWebView(htmlMessage.getHtmlContent());
            }

            @Override
            public void onFailure(String errorResponse, Throwable e) {
                mProgressBar.setVisibility(View.GONE);
                ((NavigationDrawerActivity) getActivity()).displayAlertDialog(getString(R.string.error_title), errorResponse, null, null);
            }
        });
    }

    public void updateWebView(String result) {

        mWebView.setBackgroundColor(Color.WHITE);
        if (android.os.Build.VERSION.SDK_INT >= 11) mWebView.setLayerType(
                WebView.LAYER_TYPE_SOFTWARE, null);

        mWebView.loadDataWithBaseURL("file:///android_asset/", result, "text/html", "utf-8", null);
    }

}
