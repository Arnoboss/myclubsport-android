package eu.boss.mobilesport.fffactivities;

import android.os.Bundle;

import eu.boss.mobilesport.util.Constant;

public class AgendaActivity extends WebViewActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        displayMenuButton = true;
        this.mUrl = Constant.AGENDA_URL;
    }
}
