package eu.boss.mobilesport.fffactivities;

import android.os.Bundle;

import eu.boss.mobilesport.util.Constant;

public class ResultsActivity extends WebViewActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        displayMenuButton = true;
        this.mUrl = Constant.RESULTS_URL;
    }

}
