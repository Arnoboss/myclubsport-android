package eu.boss.mobilesport.stats;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import eu.boss.library.externalmodel.PlayerListMessage;
import eu.boss.mobilesport.R;

/**
 * Fragment containing a simple webView
 */
public class StatsFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String TEAM = "team";
    private static final String PLAYER_LIST = "playerList";

    private String team;
    private PlayerListMessage mPlayerListMessage;
    private ListView mListView;
    private StatsListAdapter mListAdapter;

    public StatsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param team team Name
     * @return A new instance of fragment WebViewFragment.
     */
    public static StatsFragment newInstance(String team, PlayerListMessage playerListMessage) {
        StatsFragment fragment = new StatsFragment();
        Bundle args = new Bundle();
        args.putString(TEAM, team);
        args.putSerializable(PLAYER_LIST, playerListMessage);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            team = getArguments().getString(TEAM);
            mPlayerListMessage = (PlayerListMessage) getArguments().getSerializable(PLAYER_LIST);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_list_view, container, false);
        // Inflate the layout for this fragment
        mListView = (ListView) mainView.findViewById(R.id.listViewFragment);
        mListAdapter = new StatsListAdapter(container.getContext(), mPlayerListMessage, 0);

        final View header = inflater.inflate(R.layout.stats_header, null);
        mListView.addHeaderView(header);
        mListView.setAdapter(mListAdapter);

        View.OnClickListener sortClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case (R.id.statsHeaderPlayer):
                        mListAdapter.setSortBy(0);
                        break;
                    case (R.id.statsHeaderPlayed):
                        mListAdapter.setSortBy(1);
                        break;
                    case (R.id.statsHeaderGoals):
                        mListAdapter.setSortBy(2);
                        break;
                    case (R.id.statsHeaderAssists):
                        mListAdapter.setSortBy(3);
                        break;
                    case (R.id.statsHeaderYellowCards):
                        mListAdapter.setSortBy(4);
                        break;
                    case (R.id.statsHeaderRedCards):
                        mListAdapter.setSortBy(5);
                        break;
                    default:
                        break;
                }
                setHeaderTVNormal(header);
                ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
                ((TextView) v).setTypeface(null, Typeface.BOLD);
                mListAdapter.sortList();
                mListAdapter.notifyDataSetChanged();
            }
        };

        header.findViewById(R.id.statsHeaderPlayer).setOnClickListener(sortClickListener);
        header.findViewById(R.id.statsHeaderPlayed).setOnClickListener(sortClickListener);
        header.findViewById(R.id.statsHeaderGoals).setOnClickListener(sortClickListener);
        header.findViewById(R.id.statsHeaderAssists).setOnClickListener(sortClickListener);
        header.findViewById(R.id.statsHeaderYellowCards).setOnClickListener(sortClickListener);
        header.findViewById(R.id.statsHeaderRedCards).setOnClickListener(sortClickListener);

        return mainView;

    }

    private void setHeaderTVNormal(View headerView) {
        ((TextView) headerView.findViewById(R.id.statsHeaderPlayer)).setTypeface(null, Typeface.NORMAL);
        ((TextView) headerView.findViewById(R.id.statsHeaderPlayer)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        ((TextView) headerView.findViewById(R.id.statsHeaderPlayed)).setTypeface(null, Typeface.NORMAL);
        ((TextView) headerView.findViewById(R.id.statsHeaderPlayed)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        ((TextView) headerView.findViewById(R.id.statsHeaderGoals)).setTypeface(null, Typeface.NORMAL);
        ((TextView) headerView.findViewById(R.id.statsHeaderGoals)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        ((TextView) headerView.findViewById(R.id.statsHeaderAssists)).setTypeface(null, Typeface.NORMAL);
        ((TextView) headerView.findViewById(R.id.statsHeaderAssists)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        ((TextView) headerView.findViewById(R.id.statsHeaderYellowCards)).setTypeface(null, Typeface.NORMAL);
        ((TextView) headerView.findViewById(R.id.statsHeaderYellowCards)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        ((TextView) headerView.findViewById(R.id.statsHeaderRedCards)).setTypeface(null, Typeface.NORMAL);
        ((TextView) headerView.findViewById(R.id.statsHeaderRedCards)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
    }

}
