package eu.boss.mobilesport.stats;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;

import eu.boss.library.externalmodel.PlayerListMessage;
import eu.boss.library.externalmodel.PlayerMessage;
import eu.boss.mobilesport.R;

/**
 * Adapter to fill stats listView
 * Created by Arnaud on 10/05/2015.
 */
public class StatsListAdapter extends BaseAdapter {

    private PlayerListMessage mPlayerListMessage;
    private LayoutInflater inflater;
    private Context mContext;
    private int mSortBy = -1;
    // type de tri: 0= ASC; 1 = DESC
    private int mSortType = 1;


    public StatsListAdapter(Context context, PlayerListMessage playerListMessage, int sortBy) {
        inflater = LayoutInflater.from(context);
        mContext = context;
        mPlayerListMessage = playerListMessage;
        mSortBy = sortBy;
    }


    @Override
    public int getCount() {
        if (mPlayerListMessage == null || mPlayerListMessage.getPlayers() == null)
            return 0;
        return mPlayerListMessage.getPlayers().size();
    }

    @Override
    public Object getItem(int position) {
        return mPlayerListMessage.getPlayers().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        PlayerMessage playerMessage = mPlayerListMessage.getPlayers().get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.stats_item, null);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_stats_name);
            holder.tvPos = (TextView) convertView.findViewById(R.id.tv_stats_pos);
            holder.tvAge = (TextView) convertView.findViewById(R.id.tv_stats_age);
            holder.tvMatchs = (TextView) convertView.findViewById(R.id.tv_stats_matchs);
            holder.tvGoals = (TextView) convertView.findViewById(R.id.tv_stats_goals);
            holder.tvAssists = (TextView) convertView.findViewById(R.id.tv_stats_assists);
            holder.tvYellowCards = (TextView) convertView.findViewById(R.id.tv_stats_yellow_cards);
            holder.tvRedCards = (TextView) convertView.findViewById(R.id.tv_stats_red_cards);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        holder.tvName.setText(playerMessage.getName());
        holder.tvPos.setText(playerMessage.getPosition().name());
        holder.tvAge.setText("" + playerMessage.getAge() + " " + mContext.getString(R.string.years_old));
        if ((playerMessage.getTeamStats() != null) && (playerMessage.getTeamStats().size() > 0)) {
            holder.tvMatchs.setText("" + playerMessage.getTeamStats(0).getGamesPlayed());
            holder.tvGoals.setText("" + playerMessage.getTeamStats(0).getGoals());
            holder.tvAssists.setText("" + playerMessage.getTeamStats(0).getAssists());
            holder.tvYellowCards.setText("" + playerMessage.getTeamStats(0).getYellowCards());
            holder.tvRedCards.setText("" + playerMessage.getTeamStats(0).getRedCards());
        }

        return convertView;
    }

    public void addAll(PlayerListMessage list) {
        this.mPlayerListMessage.getPlayers().addAll(list.getPlayers());
        notifyDataSetChanged();
    }

    //BUG android 5.0: rend invisible les divider de la listview !!
    //workaround: les items sont disabled
    //@Override
    //public boolean isEnabled(int position) {
    //  return false;
    //}

    public void clear() {
        mPlayerListMessage.getPlayers().clear();
    }

    public void replace(PlayerListMessage list) {
        clear();
        addAll(list);
    }

    public PlayerMessage getPlayer(int position) {
        return mPlayerListMessage.getPlayers().get(position);
    }

    public void sortList() {
        switch (mSortBy) {
            case 0:
                Collections.sort(mPlayerListMessage.getPlayers(), new Comparator<PlayerMessage>() {
                    public int compare(PlayerMessage p1, PlayerMessage p2) {
                        if (mSortType == 0)
                            return p2.getName().compareToIgnoreCase(p1.getName());
                        else
                            return p1.getName().compareToIgnoreCase(p2.getName());
                    }
                });
                break;

            case 1:
                Collections.sort(mPlayerListMessage.getPlayers(), new Comparator<PlayerMessage>() {
                    public int compare(PlayerMessage p1, PlayerMessage p2) {
                        if (mSortType == 1)
                            return (p2.getTeamStats().get(0).getGamesPlayed() - p1.getTeamStats().get(0).getGamesPlayed());
                        else
                            return (p1.getTeamStats().get(0).getGamesPlayed() - p2.getTeamStats().get(0).getGamesPlayed());
                    }
                });
                break;
            case 2:
                Collections.sort(mPlayerListMessage.getPlayers(), new Comparator<PlayerMessage>() {
                    public int compare(PlayerMessage p1, PlayerMessage p2) {
                        if (mSortType == 1)
                            return (p2.getTeamStats().get(0).getGoals() - p1.getTeamStats().get(0).getGoals());
                        else
                            return (p1.getTeamStats().get(0).getGoals() - p2.getTeamStats().get(0).getGoals());
                    }
                });
                break;
            case 3:
                Collections.sort(mPlayerListMessage.getPlayers(), new Comparator<PlayerMessage>() {
                    public int compare(PlayerMessage p1, PlayerMessage p2) {
                        if (mSortType == 1)
                            return (p2.getTeamStats().get(0).getAssists() - p1.getTeamStats().get(0).getAssists());
                        else
                            return (p1.getTeamStats().get(0).getAssists() - p2.getTeamStats().get(0).getAssists());
                    }
                });
                break;
            case 4:
                Collections.sort(mPlayerListMessage.getPlayers(), new Comparator<PlayerMessage>() {
                    public int compare(PlayerMessage p1, PlayerMessage p2) {
                        if (mSortType == 1)
                            return (p2.getTeamStats().get(0).getYellowCards() - p1.getTeamStats().get(0).getYellowCards());
                        else
                            return (p1.getTeamStats().get(0).getYellowCards() - p2.getTeamStats().get(0).getYellowCards());
                    }
                });
                break;
            case 5:
                Collections.sort(mPlayerListMessage.getPlayers(), new Comparator<PlayerMessage>() {
                    public int compare(PlayerMessage p1, PlayerMessage p2) {
                        if (mSortType == 1)
                            return (p2.getTeamStats().get(0).getRedCards() - p1.getTeamStats().get(0).getRedCards());
                        else
                            return (p1.getTeamStats().get(0).getRedCards() - p2.getTeamStats().get(0).getRedCards());
                    }
                });
                break;

            default:
                break;

        }
    }

    public int getSortBy() {
        return mSortBy;
    }

    public void setSortBy(int sortBy) {
        //Si on vient de trier la même colonne, on inverse le type de tri (ASC et DESC)
        if (sortBy == mSortBy) {
            if (mSortType == 0)
                mSortType = 1;
            else
                mSortType = 0;
        } else
            mSortType = 1;
        this.mSortBy = sortBy;
    }

    private class ViewHolder {

        TextView tvName;
        TextView tvPos;
        TextView tvAge;
        TextView tvMatchs;
        TextView tvGoals;
        TextView tvAssists;
        TextView tvYellowCards;
        TextView tvRedCards;
    }


}
