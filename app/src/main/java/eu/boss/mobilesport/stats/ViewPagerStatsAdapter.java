package eu.boss.mobilesport.stats;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import eu.boss.library.externalmodel.Category;
import eu.boss.library.externalmodel.PlayerListMessage;
import eu.boss.library.externalmodel.PlayerMessage;
import eu.boss.library.externalmodel.TeamNameMessage;
import eu.boss.library.externalmodel.TeamStatsMessage;
import eu.boss.mobilesport.R;

/**
 * Created by Arnaud on 08/05/2015.
 */
public class ViewPagerStatsAdapter extends FragmentPagerAdapter {

    // Tab Titles
    private String tabTitles[];
    private StatsFragment[] fragments;
    private PlayerListMessage[] mPLists;
    private int numberOfPages;
    private Category category;


    public ViewPagerStatsAdapter(FragmentManager fm, Category cat, int numberOfTeamInCategory, PlayerListMessage playerListMessage, Context c) {
        super(fm);
        //SI on a une seule équipe, pas besoin d'afficher de total
        if (numberOfTeamInCategory == 1)
            this.numberOfPages = 1;
        else
            this.numberOfPages = numberOfTeamInCategory + 1;
        this.category = cat;
        this.tabTitles = new String[numberOfPages];
        this.fragments = new StatsFragment[numberOfPages];
        this.mPLists = new PlayerListMessage[numberOfPages];

        if (numberOfPages > 1) {
            tabTitles[0] = c.getString(R.string.total) + " " + category.getName();
            for (int i = 1; i < numberOfPages; i++) {
                tabTitles[i] = c.getString(R.string.team) + " " + TeamNameMessage.values()[i - 1].name();
            }
        } else {
            tabTitles[0] = category.getName();
        }

        if (numberOfPages == 1) {
            //Si on a une seule équipe, pas besoin de calculer, on affiche les stats de cette équipe
            mPLists[0] = playerListMessage;
        } else {
            mPLists[0] = new PlayerListMessage();
            //On divise la liste pour qu'elle soit plus lisible sur le smartphone lorsqu'il y a plusieurs équipes.
            for (PlayerMessage p : playerListMessage.getPlayers()) {
                PlayerMessage pClub = new PlayerMessage();
                pClub = copyPlayerInfos(p, pClub, null);

                //parcours de toutes les stats. Si un joueur n'a pas joué dans une des équipes, on ne l'ajoute pas pour améliorer la lisibilité
                for (TeamStatsMessage ts : p.getTeamStats()) {
                    if (ts.getGamesPlayed() > 0) {
                        for (int i = 1; i < numberOfPages; i++) {
                            if (ts.getTeamName() == TeamNameMessage.values()[i - 1]) {
                                PlayerMessage pA = new PlayerMessage();
                                pA = copyPlayerInfos(p, pA, ts);
                                if (mPLists[i] == null)
                                    mPLists[i] = new PlayerListMessage();
                                mPLists[i].addPlayer(pA);
                                pClub = totalTeamStats(pClub, ts);
                                break;
                            }
                        }
                    }
                }
                mPLists[0].addPlayer(pClub);
            }
        }

    }

    private PlayerMessage totalTeamStats(PlayerMessage p, TeamStatsMessage ts) {
        if ((p.getTeamStats() == null) || (p.getTeamStats().size() < 1)) {
            List<TeamStatsMessage> tsList = new ArrayList<>();
            tsList.add(ts);
            p.setTeamStats(tsList);
        } else {
            TeamStatsMessage oldTs = p.getTeamStats().get(0);
            TeamStatsMessage newTs = new TeamStatsMessage();
            newTs.setGamesPlayed(oldTs.getGamesPlayed() + ts.getGamesPlayed());
            newTs.setGoals(oldTs.getGoals() + ts.getGoals());
            newTs.setAssists(oldTs.getAssists() + ts.getAssists());
            newTs.setYellowCards(oldTs.getYellowCards() + ts.getYellowCards());
            newTs.setRedCards(oldTs.getRedCards() + ts.getRedCards());
            p.getTeamStats().add(0, newTs);
        }
        return p;

    }

    private PlayerMessage copyPlayerInfos(PlayerMessage pBase, PlayerMessage pToFill, TeamStatsMessage stats) {
        pToFill.setName(pBase.getName());
        pToFill.setAge(pBase.getAge());
        pToFill.setPosition(pBase.getPosition());
        pToFill.setSeason(pBase.getSeason());
        if (stats != null) {
            List<TeamStatsMessage> teamStatMessages = new ArrayList<>();
            teamStatMessages.add(stats);
            pToFill.setTeamStats(teamStatMessages);
        }
        return pToFill;
    }

    @Override
    public int getCount() {
        return numberOfPages;
    }

    @Override
    public Fragment getItem(int position) {
        if (fragments[position] == null)
            fragments[position] = StatsFragment.newInstance(TeamNameMessage.values()[position].name(), mPLists[position]);
        return fragments[position];
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
