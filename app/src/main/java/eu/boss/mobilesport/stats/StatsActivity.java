package eu.boss.mobilesport.stats;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.library.externalmodel.Category;
import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.externalmodel.PlayerListMessage;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.library.util.PrefsManager;
import eu.boss.library.util.Util;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.CoteauxApplication;
import eu.boss.mobilesport.main.ViewPagerTabActivity;
import eu.boss.mobilesport.util.Constant;

public class StatsActivity extends ViewPagerTabActivity {

    private static String PREFS_STATS_SEEN = "sortTutoHasBeenSeen";
    private PlayerListMessage mPlayerListMessage;
    private Category category;
    private int numberOfTeamInCategory = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findViewById(R.id.fl_progress_bar).setVisibility(View.VISIBLE);
        findViewById(R.id.pager).setVisibility(View.GONE);
        displayMenuButton = true;
    }


    @Override
    public void onStart() {
        super.onStart();
        performTask();
    }

    private void performTask() {

        HttpRequestManager requestManager = new HttpRequestManager();
        requestManager.asyncHttpRequest(StatsActivity.this, null, String.format(Constant.PLAYERS_URL, category.name(), Util.currentSeasonDate()), HttpMethod.GET, new ResponseHttpHandler() {
            @Override
            public void onSuccess(GenericResponseMessage<Object> response) {
                findViewById(R.id.fl_progress_bar).setVisibility(View.GONE);
                findViewById(R.id.pager).setVisibility(View.VISIBLE);
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    mPlayerListMessage = mapper.convertValue(response.getPayLoad(), PlayerListMessage.class);
                    initViewPager();
                } catch (Exception e) {
                    e.printStackTrace();
                    displayAlertDialog(getString(R.string.error_title), getString(R.string.error_request), null, null);
                }
                if (!PrefsManager.isTrue(StatsActivity.this, PREFS_STATS_SEEN)) {
                    startActivity(new Intent(StatsActivity.this, SortStatsTutoActivity.class));
                    PrefsManager.saveBool(StatsActivity.this, PREFS_STATS_SEEN, true);
                }
            }

            @Override
            public void onFailure(String errorResponse, Throwable e) {
                findViewById(R.id.fl_progress_bar).setVisibility(View.GONE);
                findViewById(R.id.pager).setVisibility(View.VISIBLE);
                displayAlertDialog(getString(R.string.error_title), errorResponse, null, null);
            }
        });
    }

    @Override
    protected void initViewPager() {
        category = (Category) getIntent().getExtras().getSerializable(Constant.CATEGORY);
        numberOfTeamInCategory = ((CoteauxApplication) getApplication()).getNumberOfTeamInCategory(category);
        if (mPlayerListMessage != null) {
            // Set the ViewPagerAdapter into ViewPager
            ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
            viewPager.setAdapter(new ViewPagerStatsAdapter(getFragmentManager(), category, numberOfTeamInCategory, mPlayerListMessage, StatsActivity.this));
        }
    }

}
