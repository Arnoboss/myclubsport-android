package eu.boss.mobilesport.stats;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import eu.boss.mobilesport.R;

public class SortStatsTutoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort_stats_tuto);

        findViewById(R.id.btnTutoOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


}
