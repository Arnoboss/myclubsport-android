package eu.boss.mobilesport.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import eu.boss.library.externalmodel.Category;
import eu.boss.mobilesport.R;

/**
 * Created by Arnaud on 02/01/2016.
 */
public class CategoriesAdapter extends BaseAdapter {

    private ArrayList<Category> mCategoryList;
    private LayoutInflater inflater;


    public CategoriesAdapter(Context context, ArrayList<Category> categoryList
    ) {
        inflater = LayoutInflater.from(context);
        mCategoryList = categoryList;
    }


    @Override
    public int getCount() {
        return mCategoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return mCategoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        Category category = mCategoryList.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.category_item, null);
            holder.tvCategoryName = (TextView) convertView.findViewById(R.id.tvCategoryName);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();
        holder.tvCategoryName.setText(category.getName());

        return convertView;
    }

    public void addAll(ArrayList<Category> list) {
        mCategoryList.addAll(list);
        notifyDataSetChanged();
    }

    public void clear() {
        mCategoryList.clear();
    }

    public void replace(ArrayList<Category> list) {
        clear();
        addAll(list);
    }

    public Category getCategory(int position) {
        return mCategoryList.get(position);
    }

    private class ViewHolder {
        TextView tvCategoryName;
    }
}
