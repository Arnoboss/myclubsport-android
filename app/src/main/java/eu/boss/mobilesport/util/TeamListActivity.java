package eu.boss.mobilesport.util;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Iterator;

import eu.boss.library.externalmodel.Category;
import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.externalmodel.TeamListMessage;
import eu.boss.library.externalmodel.TeamMessage;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.fffactivities.CalendarActivityPager;
import eu.boss.mobilesport.fffactivities.RankingActivityPager;
import eu.boss.mobilesport.main.CoteauxApplication;
import eu.boss.mobilesport.main.NavigationDrawerActivity;
import eu.boss.mobilesport.stats.StatsActivity;

/**
 * Activity listview affichant la liste des categories
 */
public class TeamListActivity extends NavigationDrawerActivity implements AdapterView.OnItemClickListener {


    ArrayList<Category> categoryList = new ArrayList<>();
    private String menuSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_list);
        displayMenuButton = true;
        //On récupère le param pour savoir sur quel élément du menu l'utilisateur a cliqué
        this.menuSelected = getIntent().getExtras().getString(Constant.MENUSELECTED);
        if (menuSelected.equals(Constant.CALENDAR))
            setTitle(R.string.title_activity_calendar);
        else if (menuSelected.equals(Constant.RANK)) setTitle(R.string.title_activity_ranking);
        else if (menuSelected.equals(Constant.STATS)) setTitle(R.string.title_activity_stats);

        //Garde fou: en cas de crash ou de bug, si l'a
        if (((CoteauxApplication) getApplication()).getTeamList() != null)
            initList();
        else
            retrieveTeamAndCategoryList();
    }

    private void initList() {
        //On récupère la liste des équipes et on crée une liste contenant uniquement les catégories.
        Iterator<TeamMessage> it = ((CoteauxApplication) getApplication()).getTeamList().getTeams().iterator();
        while (it.hasNext()) {
            Category c = Category.valueOf(it.next().getCategory());
            if (!categoryList.contains(c)) {
                categoryList.add(c);
            }
        }

        ListView lvCategories = (ListView) findViewById(R.id.lvCategories);
        lvCategories.setAdapter(new CategoriesAdapter(TeamListActivity.this, categoryList));
        lvCategories.setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView<?> l, View v, int position, long id) {

        // Then you start a new Activity via Intent
        v.setSelected(true);
        Intent intent = new Intent();
        if (menuSelected.equals(Constant.CALENDAR))
            intent.setClass(this, CalendarActivityPager.class);
        else if (menuSelected.equals(Constant.RANK))
            intent.setClass(this, RankingActivityPager.class);
        else if (menuSelected.equals(Constant.STATS)) intent.setClass(this, StatsActivity.class);
        intent.putExtra(Constant.CATEGORY, categoryList.get(position));
        startActivity(intent);
    }

    private void retrieveTeamAndCategoryList() {
        Log.i("TeamListActivity", "retrieveTeamAndCategoryList");
        HttpRequestManager requestManager = new HttpRequestManager();
        final ObjectMapper mapper = new ObjectMapper();
        final ProgressDialog progressDialog = new ProgressDialog(TeamListActivity.this, ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        ResponseHttpHandler handler = new ResponseHttpHandler() {
            @Override
            public void onSuccess(GenericResponseMessage<Object> response) {
                progressDialog.dismiss();
                TeamListMessage teamList = mapper.convertValue(response.getPayLoad(), TeamListMessage.class);
                ArrayList<Category> catList = new ArrayList<>();
                for (TeamMessage t : teamList.getTeams()) {
                    Category c = Category.valueOf(t.getCategory());
                    if (!catList.contains(c)) {
                        catList.add(c);
                    }
                }
                ((CoteauxApplication) getApplication()).setTeamList(teamList);
                ((CoteauxApplication) getApplication()).setCategoryList(catList);
                initList();
            }

            @Override
            public void onFailure(String errorResponse, Throwable e) {
                progressDialog.dismiss();
                Log.e("getTeamAndCat onFailure", e.toString());
                displayAlertDialog(getString(R.string.error_title), getString(R.string.no_connection), null, null);
            }
        };

        try {
            requestManager.asyncHttpRequest(getApplicationContext(), null, Constant.TEAM_LIST_URL,
                    HttpMethod.GET, handler);
        } catch (Exception e) {
            Log.e("TeamListActivity", e.getMessage());
        }
    }

}
