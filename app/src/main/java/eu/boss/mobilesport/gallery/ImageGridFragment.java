package eu.boss.mobilesport.gallery;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import eu.boss.mobilesport.R;
import eu.boss.mobilesport.util.Constant;

/**
 * Fragment affichant une grille d'images. Basé sur le sample de la library Universal Image Loader
 */
public class ImageGridFragment extends Fragment {
    protected AbsListView listView;
    protected ArrayList<String> urlImageList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_image_grid, container, false);
        listView = (GridView) rootView.findViewById(R.id.grid);
        Bundle bundle = this.getArguments();
        urlImageList = (ArrayList<String>) bundle.getSerializable(GalleryActivity.GALLERY_URL_IMAGE);

        ((GridView) listView).setAdapter(new ImageAdapter(getActivity(), urlImageList));
        listView.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), false, true));
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startImagePagerActivity(position);
            }
        });

        return rootView;
    }

    protected void startImagePagerActivity(int position) {
        //Intent intent = new Intent(getActivity(), ImageActivity.class);
        //startActivity(intent);

        Intent intent = new Intent(getActivity(), ImageActivityPager.class);
        intent.putExtra(GalleryActivity.GALLERY_URL_IMAGE, new ArrayList<String>(urlImageList));
        intent.putExtra(Constant.IMAGE_POSITION, position);
        startActivity(intent);
    }

    private static class ImageAdapter extends BaseAdapter {


        private LayoutInflater inflater;
        private ArrayList<String> urlImageList;
        private DisplayImageOptions options;

        ImageAdapter(Context context, ArrayList<String> urlList) {
            inflater = LayoutInflater.from(context);
            urlImageList = urlList;
            options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.ic_nb)
                    .showImageForEmptyUri(R.drawable.ic_nb)
                    .showImageOnFail(R.drawable.ic_nb)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();
        }

        @Override
        public int getCount() {
            return urlImageList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            View view = convertView;
            if (view == null) {
                view = inflater.inflate(R.layout.item_grid_image, parent, false);
                holder = new ViewHolder();
                assert view != null;
                holder.imageView = (ImageView) view.findViewById(R.id.image);
                holder.progressBar = (ProgressBar) view.findViewById(R.id.progress);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            ImageLoader.getInstance()
                    .displayImage(Constant.IMAGE_PUBLIC_PATH + urlImageList.get(position), holder.imageView, options, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            holder.progressBar.setProgress(0);
                            holder.progressBar.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    }, new ImageLoadingProgressListener() {
                        @Override
                        public void onProgressUpdate(String imageUri, View view, int current, int total) {
                            holder.progressBar.setProgress(Math.round(100.0f * current / total));
                        }
                    });

            return view;
        }
    }

    static class ViewHolder {
        ImageView imageView;
        ProgressBar progressBar;
    }

}
