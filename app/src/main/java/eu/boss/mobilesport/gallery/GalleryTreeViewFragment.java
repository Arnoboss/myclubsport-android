package eu.boss.mobilesport.gallery;

import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.ArrayList;

import eu.boss.mobilesport.R;
import eu.boss.mobilesport.views.IconTreeItemHolder;


public class GalleryTreeViewFragment extends Fragment {
    private AndroidTreeView tView;
    private eu.boss.library.externalmodel.TreeNode<String> galleryTreeNode;
    private TreeNode.TreeNodeClickListener nodeClickListener = new TreeNode.TreeNodeClickListener() {
        @Override
        public void onClick(TreeNode node, Object value) {
            if (node.getChildren().isEmpty()) {
                ArrayList<String> pathList = new ArrayList<String>();
                retrieveCompletePath(node, pathList);

                String path = "";
                for (int i = pathList.size() - 2; i >= 0; i--) {
                    path += pathList.get(i) + "/";
                }
                ((GalleryTreeViewActivity) getActivity()).showImagesInFolder(path);
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_gallery_tree_view, null, false);
        ViewGroup containerView = (ViewGroup) rootView.findViewById(R.id.container);

        Bundle bundle = this.getArguments();
        galleryTreeNode = (eu.boss.library.externalmodel.TreeNode<String>) bundle.getSerializable(GalleryTreeViewActivity.GALLERY_TREE_NODE);

        TreeNode root = TreeNode.root();
        TreeNode galleryRoot = new TreeNode(new IconTreeItemHolder.IconTreeItem(R.string.ic_folder, getString(R.string.title_activity_gallery)));

        for (eu.boss.library.externalmodel.TreeNode<String> child : galleryTreeNode.children) {
            addSubmenuOrItemIn(galleryRoot, child);
        }

        root.addChildren(galleryRoot);

        tView = new AndroidTreeView(getActivity(), root);
        tView.setDefaultAnimation(true);
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom);
        tView.setDefaultViewHolder(IconTreeItemHolder.class);
        tView.setDefaultNodeClickListener(nodeClickListener);
        //tView.setDefaultNodeLongClickListener(nodeLongClickListener);

        containerView.addView(tView.getView());

        if (savedInstanceState != null) {
            String state = savedInstanceState.getString("tState");
            if (!TextUtils.isEmpty(state)) {
                tView.restoreState(state);
            }
        }
        tView.expandAll();
        return rootView;
    }

    private void addSubmenuOrItemIn(TreeNode submenu, eu.boss.library.externalmodel.TreeNode<String> treeNode) {
        String[] splitted = treeNode.data.split("/");
        // On part du principe qu'un dossier ne peut contenir qu'un seul type de données: soit un
        // autre dossier, soit des images
        if (treeNode.children == null || treeNode.children.isEmpty()
                || isNodeContainImage(treeNode.children.get(0))) {
            TreeNode item = new TreeNode(new IconTreeItemHolder.IconTreeItem(R.string.ic_photo_library, (splitted[splitted.length - 1])));
            submenu.addChild(item);
        } else {
            TreeNode secondSubmenu = new TreeNode(new IconTreeItemHolder.IconTreeItem(R.string.ic_folder, (splitted[splitted.length - 1])));
            submenu.addChild(secondSubmenu);
            for (eu.boss.library.externalmodel.TreeNode<String> child : treeNode.children) {
                addSubmenuOrItemIn(secondSubmenu, child);
            }
        }
    }

    // On part du principe qu'un dossier ne peut contenir qu'un seul type de données: soit un
    // autre dossier, soit des images
    private boolean isNodeContainImage(eu.boss.library.externalmodel.TreeNode<String> treeNode) {
        if (treeNode.data.contains(".jpg") || treeNode.data.contains(".jpeg")
                || treeNode.data.contains(".JPG") || treeNode.data.contains(".JPEG")
                || treeNode.data.contains(".png") || treeNode.data.contains(".PNG")) return true;
        return false;
    }

    private void retrieveCompletePath(TreeNode node, ArrayList<String> pathList) {
        if (node != null && node.getValue() != null) {
            pathList.add(((IconTreeItemHolder.IconTreeItem) node.getValue()).text);
            retrieveCompletePath(node.getParent(), pathList);
        }
    }


/*    private TreeNode.TreeNodeLongClickListener nodeLongClickListener = new TreeNode.TreeNodeLongClickListener() {
        @Override
        public boolean onLongClick(TreeNode node, Object value) {
            IconTreeItemHolder.IconTreeItem item = (IconTreeItemHolder.IconTreeItem) value;
            Toast.makeText(getActivity(), "Long click: " + item.text, Toast.LENGTH_SHORT).show();
            return true;
        }
    };*/

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tState", tView.getSaveState());
    }
}
