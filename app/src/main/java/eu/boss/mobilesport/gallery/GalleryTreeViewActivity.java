package eu.boss.mobilesport.gallery;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.library.externalmodel.GalleryResponseMessage;
import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.NavigationDrawerActivity;
import eu.boss.mobilesport.util.Constant;

public class GalleryTreeViewActivity extends NavigationDrawerActivity {

    public static final String GALLERY_TREE_NODE = "galleryTreeNode";
    public static final String GALLERY_SELECTED_FOLDER = "gallerySelectedFolder";
    private GalleryResponseMessage galleryTree;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_tree_view);
        displayMenuButton = true;
        retrieveGalleryTreeFolder();
    }

    /**
     * Retrive tree folder from server
     */
    private void retrieveGalleryTreeFolder() {
        HttpRequestManager requestManager = new HttpRequestManager();
        requestManager.asyncHttpRequest(GalleryTreeViewActivity.this, null, String.format(Constant.GALLERY_URL, ""), HttpMethod.GET, new ResponseHttpHandler() {
            @Override
            public void onSuccess(GenericResponseMessage<Object> response) {
                findViewById(R.id.flpbGallery).setVisibility(View.GONE);
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    galleryTree = mapper.convertValue(response.getPayLoad(), GalleryResponseMessage.class);
                    GalleryTreeViewFragment f = new GalleryTreeViewFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(GALLERY_TREE_NODE, galleryTree.getPathTreeNode());
                    f.setArguments(bundle);
                    getFragmentManager().beginTransaction().add(R.id.galleryFragment, f).commit();
                } catch (Exception e) {
                    e.printStackTrace();
                    displayAlertDialog(getString(R.string.error_title), getString(R.string.error_request), null, null);
                }
            }

            @Override
            public void onFailure(String errorResponse, Throwable e) {
                findViewById(R.id.flpbGallery).setVisibility(View.GONE);
                displayAlertDialog(getString(R.string.error_title), errorResponse, null, null);
            }
        });
    }

    public void showImagesInFolder(String folder) {
        Intent i = new Intent(GalleryTreeViewActivity.this, GalleryActivity.class);
        i.putExtra(GALLERY_SELECTED_FOLDER, folder);
        startActivity(i);
    }

}
