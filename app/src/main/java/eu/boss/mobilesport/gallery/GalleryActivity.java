package eu.boss.mobilesport.gallery;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URLEncoder;
import java.util.ArrayList;

import eu.boss.library.externalmodel.GalleryResponseMessage;
import eu.boss.library.externalmodel.GenericResponseMessage;
import eu.boss.library.http.HttpMethod;
import eu.boss.library.http.HttpRequestManager;
import eu.boss.library.http.ResponseHttpHandler;
import eu.boss.mobilesport.R;
import eu.boss.mobilesport.main.NavigationDrawerActivity;
import eu.boss.mobilesport.util.Constant;

public class GalleryActivity extends NavigationDrawerActivity {
    public static final String GALLERY_URL_IMAGE = "galleryUrlImages";
    private GalleryResponseMessage galleryTree;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        displayMenuButton = true;
        setContentView(R.layout.activity_gallery);
        try {
            showImagesInFolder(getIntent().getExtras().getString(GalleryTreeViewActivity.GALLERY_SELECTED_FOLDER));
        } catch (Exception e) {
            displayAlertDialog(getString(R.string.error_title), getString(R.string.error_request), null, null);
        }
    }

    public void showImagesInFolder(String folderPath) throws Exception {
        HttpRequestManager requestManager = new HttpRequestManager();
        Log.d("showImagesInFolder ", folderPath);
        requestManager.asyncHttpRequest(GalleryActivity.this, null, String.format(Constant.GALLERY_URL, URLEncoder.encode(Constant.FOLDER_GALLERY + folderPath, "UTF-8")), HttpMethod.GET, new ResponseHttpHandler() {
            @Override
            public void onSuccess(GenericResponseMessage<Object> response) {
                findViewById(R.id.flpbGalleryGrid).setVisibility(View.GONE);
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    galleryTree = mapper.convertValue(response.getPayLoad(), GalleryResponseMessage.class);
                    displayGalleryGrid();
                } catch (Exception e) {
                    e.printStackTrace();
                    displayAlertDialog(getString(R.string.error_title), getString(R.string.error_request), null, null);
                }
            }

            @Override
            public void onFailure(String errorResponse, Throwable e) {
                findViewById(R.id.flpbGalleryGrid).setVisibility(View.GONE);
                displayAlertDialog(getString(R.string.error_title), errorResponse, null, null);
            }
        });
    }

    public void displayGalleryGrid() {
        Fragment fragment = new ImageGridFragment();
        Bundle b = new Bundle();
        b.putSerializable(GALLERY_URL_IMAGE, new ArrayList<String>(galleryTree.getImagesUrl()));
        fragment.setArguments(b);
        getFragmentManager().beginTransaction().replace(R.id.containerGalleryGrid, fragment, ImageGridFragment.class.getSimpleName()).commit();
    }
}
