package eu.boss.mobilesport.gallery;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import eu.boss.mobilesport.R;

public class ImageActivityPager extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_activity_pager);
        Fragment fr = new ImagePagerFragment();
        fr.setArguments(getIntent().getExtras());
        getFragmentManager().beginTransaction().replace(R.id.containerGalleryPager, fr, ImagePagerFragment.class.getSimpleName()).commit();
    }
}
